autocmd FileType sql,mysql,plsql :lua require('cmp').setup.buffer({ sources = {{ name = 'vim-dadbod-completion' }} })
let g:vim_dadbod_completion_mark = '[DB]'


let g:db_ui_tmp_query_location = '~/.local/share/db_ui_queries'
let g:db_ui_show_database_icon = 1
let g:db_ui_use_nerd_fonts = 1
let g:db_ui_execute_on_save = 0
