let g:session_dir = '~/.vim/sessions'
function SaveSession(session_dir, sess_name, bang)
    execute 'mks' . a:bang . ' ' . a:session_dir . '/' . a:sess_name
endfunction

command! -bang -nargs=1 SaveSession call SaveSession(g:session_dir, <f-args>, <q-bang>)

nmap <F1> :tab help<CR>
nmap <F2> :Startify<CR>
nmap <F4> :Ttoggle<CR>
nmap <F5> :call SaveSession(session_dir, fnamemodify('.', ':p:h:t'), '!')<CR>

map zp :setlocal spell spelllang=pl<CR>
map ze :setlocal spell spelllang=en_us<CR>
map zn :setlocal nospell<CR>

map <Left> <Nop>
map <Right> <Nop>
map <Up> <Nop>
map <Down> <Nop>

map <C-j> <PageDown>
map <C-k> <PageUp>
map <C-h> <Home>
map <C-l> <End>

" Copy selection (with/without register) and remove indention and new lines in
" the middle (except last one). Useful for copying text to browser when the tw
" setting is low.
" vmap <silent> t y:call setreg(v:register, substitute(substitute(getreg(v:register), "\\n\ *\\(\\w*\\)", " \\1", "g"), "^ \\+", "", ""))<CR>

nmap <leader>tw :Twilight<CR>
nmap <leader>zm :ZenMode<CR>
nmap <C-n> :noh<CR>

nmap <C-w>c :q <bar> wincmd p<CR>
nmap <C-w><C-c> :q <bar> wincmd p<CR>
