-- Disable arrow keys
vim.keymap.set("", "<Left>", "")
vim.keymap.set("", "<Right>", "")
vim.keymap.set("", "<Up>", "")
vim.keymap.set("", "<Down>", "")

vim.keymap.set("", "<C-j>", "<PageDown>")
vim.keymap.set("", "<C-k>", "<PageUp>")
vim.keymap.set("", "<C-h>", "<Home>")
vim.keymap.set("", "<C-l>", "<End>")

-- Quickly disable highlight
vim.keymap.set("n", "<C-n>", "<cmd>:noh<cr>")

-- Spelling-related mappings
vim.keymap.set("n", "zp", "<cmd>:setlocal spell spelllang=pl<cr>")
vim.keymap.set("n", "ze", "<cmd>:setlocal spell spelllang=en_us<cr>")
vim.keymap.set("n", "zn", "<cmd>:setlocal nospell<cr>")

-- Close window and go back to the previous focused one
vim.keymap.set("n", "<C-w>c", "<cmd>:q <bar> wincmd p<cr>")
vim.keymap.set("n", "<C-w><C-c>", "<cmd>:q <bar> wincmd p<cr>")

-- Map shift-space to just space, as without that it does somekind of undo in the prompt.
vim.keymap.set("t", "<S-space>", "<space>")
