return {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    lazy = false,
    opts = {
        scope = {
            enabled = false,
        },
        exclude = {
            filetypes = {
                'help', 'startify', 'NvimTree', 'floggraph', 'fugitive',
                'man', 'neoterm', 'dbui', 'dashboard', ''
            },
        },
        indent = {
            tab_char = "▎",
        }
    },
}
