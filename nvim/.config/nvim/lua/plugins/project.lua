return {
    "ahmedkhalf/project.nvim",
    lazy = false,
    config = function()
        require("project_nvim").setup({
            detection_methods = { "pattern" },
            patterns = { ".git", ".hg", ".bzr", ".svn" },
            silent_chdir = false,
            scope_chdir = 'win'
        })
    end,
}
