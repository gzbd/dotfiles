return {
    "hrsh7th/nvim-cmp",
    dependencies = {
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-cmdline",
        "hrsh7th/cmp-nvim-lua",
        "davidsierradz/cmp-conventionalcommits",
        "quangnguyen30192/cmp-nvim-ultisnips",
        {
            "onsails/lspkind-nvim",
            opts = {
                symbol_map = {
                    Text = "",
                    String = "",
                    Array = "",
                    Object = "󰅩",
                    Namespace = "",
                    Method = "𝕞",
                    Function = "󰊕",
                    Constructor = "",
                    Field = "",
                    Variable = "󰫧",
                    Class = "",
                    Interface = "",
                    Module = "",
                    Property = "",
                    Unit = "",
                    Value = "",
                    Enum = "",
                    Keyword = "",
                    Snippet = "",
                    Color = "",
                    File = "",
                    Reference = "",
                    Folder = "",
                    EnumMember = "",
                    Constant = "",
                    Struct = "",
                    Event = "",
                    Operator = "",
                    TypeParameter = "",
                    Copilot = "🤖",
                    Boolean = "",
                }
            },
            config = function(_, opts)
                require("lspkind").init(opts)
            end
        }
    },
    lazy = false,
    config = function()
        local cmp = require 'cmp'
        local lspkind = require 'lspkind'

        cmp.setup({
            preselect = cmp.PreselectMode.None,
            window = {
                completion = cmp.config.window.bordered({
                    winhighlight =
                    "Normal:Normal,FloatBorder:FloatBorder,CursorLine:Visual,Search:None",
                }),
                documentation = cmp.config.window.bordered({
                    winhighlight =
                    "Normal:Normal,FloatBorder:FloatBorder,CursorLine:Visual,Search:None",
                }),
            },
            snippet = {
                expand = function(args)
                    vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
                end,
            },
            formatting = {
                format = lspkind.cmp_format({
                    mode = "symbol_text",
                    menu = ({
                        buffer = "[Buffer]",
                        nvim_lsp = "[LSP]",
                        ultisnips = "[UltiSnips]",
                        nvim_lua = "[Lua]",
                        path = "[Path]",
                        ['vim-dadbod-completion'] = "[DB]",
                    })
                }),
            },
            view = {
                entries = "custom"
            },
            mapping = {
                ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
                ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
                ['<C-x><C-o>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
                ['<C-x><C-s>'] = cmp.mapping(cmp.mapping.complete({
                        config = {
                            sources = { { name = 'ultisnips' } } }
                    }),
                    { 'i', 'c' }),
                ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
                ['<C-e>'] = cmp.mapping({
                    i = cmp.mapping.abort(),
                    c = cmp.mapping.close(),
                }),
                ['<C-n>'] = cmp.mapping({
                    c = function()
                        if cmp.visible() then
                            cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
                        else
                            vim.api.nvim_feedkeys(t('<Down>'), 'n', true)
                        end
                    end,
                    i = function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
                        else
                            fallback()
                        end
                    end
                }),
                ['<C-p>'] = cmp.mapping({
                    c = function()
                        if cmp.visible() then
                            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
                        else
                            vim.api.nvim_feedkeys(t('<Up>'), 'n', true)
                        end
                    end,
                    i = function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
                        else
                            fallback()
                        end
                    end
                }),
                ['<C-space>'] = cmp.mapping(
                    cmp.mapping.confirm({ select = true, behavior = cmp.ConfirmBehavior.Replace }), { 'i', 'c' }),
            },
            sources = {
                { name = 'nvim_lsp',  group_index = 1 },
                { name = 'nvim_lua',  group_index = 1 },
                { name = 'ultisnips', group_index = 1 },
                { name = 'path',      group_index = 2 },
                { name = 'buffer',    group_index = 3 },
            },
            experimental = {
                ghost_text = true,
            },
        })

        -- `/` cmdline setup.
        cmp.setup.cmdline('/', {
            sources = {
                { name = 'buffer' }
            }
        })
        -- `:` cmdline setup.
        cmp.setup.cmdline(':', {
            sources = cmp.config.sources({
                { name = 'path' }
            }, {
                { name = 'cmdline' }
            })
        })
    end
}
