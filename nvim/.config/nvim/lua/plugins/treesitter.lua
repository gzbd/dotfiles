return {
    'nvim-treesitter/nvim-treesitter',
    version = false, -- last release is way too old and doesn't work on Windows
    build = ":TSUpdate",
    event = { "BufReadPost", "BufNewFile" },
    cmd = { "TSUpdateSync" },
    dependencies = {
        'nvim-treesitter/nvim-treesitter-textobjects',
    },
    opts = {
        ensure_installed = "all",
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
        },
        matchup = {
            enable = true,
        },
        textobjects = {
            lsp_interop = {
                enable = true,
                border = 'none',
                floating_preview_opts = {},
                peek_definition_code = {
                    ["<leader>df"] = "@function.outer",
                    ["<leader>dF"] = "@class.outer",
                },
            },
            select = {
                enable = true,
                lookahead = true,
                keymaps = {
                    ["afu"] = "@function.outer",
                    ["ifu"] = "@function.inner",
                    ["afc"] = "@call.outer",
                    ["ifc"] = "@call.inner",
                    ["acl"] = "@class.outer",
                    ["icl"] = "@class.inner",
                    ["aco"] = "@conditional.outer",
                    ["ico"] = "@conditional.inner",
                    ["apa"] = "@parameter.outer",
                    ["ipa"] = "@parameter.inner",
                    ["alo"] = "@loop.outer",
                    ["ilo"] = "@loop.inner",
                    ["abl"] = "@block.outer",
                    ["ibl"] = "@block.inner",
                    ["afd"] = { query = "@field", query_group = "highlights", desc = "A field" },
                    ["asc"] = { query = "@scope", query_group = "locals", desc = "A scope" },
                },
            },
            move = {
                enable = true,
                set_jumps = true,
                goto_next_start = {
                    ["]fu"] = "@function.outer",
                    ["]fc"] = "@call.inner",
                    ["]pa"] = "@parameter.inner",
                    ["]cl"] = "@class.outer",
                    ["]bl"] = "@block.outer",
                    ["]z"] = { query = "@fold", query_group = "folds", desc = "Next fold" },
                    ["]fd"] = { query = "@field", query_group = "highlights", desc = "Next field" },
                    ["]sc"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
                },
                goto_next_end = {
                    ["]FU"] = "@function.outer",
                    ["]FC"] = "@call.inner",
                    ["]PA"] = "@parameter.inner",
                    ["]CL"] = "@class.outer",
                    ["]BL"] = "@block.outer",
                    ["]SC"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
                },
                goto_previous_start = {
                    ["[fu"] = "@function.outer",
                    ["[fc"] = "@call.inner",
                    ["[pa"] = "@parameter.inner",
                    ["[cl"] = "@class.outer",
                    ["[bl"] = "@block.outer",
                    ["[z"] = { query = "@fold", query_group = "folds", desc = "Previous fold" },
                    ["[fd"] = { query = "@field", query_group = "highlights", desc = "Previous field" },
                    ["[sc"] = { query = "@scope", query_group = "locals", desc = "Previous scope" },
                },
                goto_previous_end = {
                    ["[FU"] = "@function.outer",
                    ["[FC"] = "@call.inner",
                    ["[PA"] = "@parameter.inner",
                    ["[CL"] = "@class.outer",
                    ["[BL"] = "@block.outer",
                    ["[SC"] = { query = "@scope", query_group = "locals", desc = "Previous scope" },
                },
            },
            swap = {
                enable = true,
                swap_next = {
                    ["<leader>sp"] = "@parameter.inner",
                },
                swap_previous = {
                    ["<leader>sP"] = "@parameter.inner",
                },
            },
        },
    },
    keys = {
        {
            ";",
            function()
                local ts_repeat_move = require "nvim-treesitter.textobjects.repeatable_move"
                ts_repeat_move.repeat_last_move()
            end,
            mode = { "n", "x", "o" },
            desc = "Repeat last move"
        },
        {
            ",",
            function()
                local ts_repeat_move = require "nvim-treesitter.textobjects.repeatable_move"
                ts_repeat_move.repeat_last_move_opposite()
            end,
            mode = { "n", "x", "o" },
            desc = "Repeat last move opposite"
        },
    },
    config = function(_, opts)
        require("nvim-treesitter.configs").setup(opts)

        local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
        parser_config.bazgranina = {
            install_info = {
                url = "~/Repositories/Private/bazgranina/treesitter", -- local path or git repo
                files = { "src/parser.c", "src/scanner.cc" },
                -- optional entries:
                branch = "main",                       -- default branch in case of git repo if different from master
                generate_requires_npm = false,         -- if stand-alone parser without npm dependencies
                requires_generate_from_grammar = true, -- if folder contains pre-generated src/parser.c
            },
            filetype = "bazgranina",                   -- if filetype does not match the parser name
        }
    end
}
