return {
    'glepnir/dashboard-nvim',
    event = 'VimEnter',
    config = function()
        require('dashboard').setup({
            shortcut_type = 'number',
            config = {
                header = {
                    "",
                    " ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗",
                    " ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║",
                    " ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║",
                    " ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║",
                    " ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║",
                    " ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝",
                    "",
                },
                footer = {},
                shortcut = {
                    { desc = '󰩈 Quit', group = 'Error', action = 'quit', key = 'q' },
                    { desc = '󰊳 Update', group = '@function', action = 'Lazy update', key = 'u' },
                    {
                        icon = ' ',
                        desc = 'Repositories',
                        group = '@function',
                        action = 'normal \\rl',
                        key = 'r',
                    },
                    {
                        icon = ' ',
                        desc = 'Sessions',
                        group = '@function',
                        action = 'normal \\sl',
                        key = 's',
                    },
                    {
                        icon = ' ',
                        desc = 'Empty',
                        group = 'Title',
                        action = 'new | only!',
                        key = 'e',
                    },
                },
                project = { enable = true, limit = 5, action = 'Telescope file_browser cwd=' },
            }
        })
        vim.cmd([[hi link DashboardHeader Title]])
    end,
    dependencies = { { 'nvim-tree/nvim-web-devicons' } }
}
