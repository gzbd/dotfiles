return {
    "lewis6991/gitsigns.nvim",
    lazy = false,
    opts = {
        signs_staged_enable = false,
        on_attach = function(bufnr)
            vim.api.nvim_buf_set_keymap(bufnr, 'n', '[h', '<cmd>lua require"gitsigns".prev_hunk()<CR>', {})
            vim.api.nvim_buf_set_keymap(bufnr, 'n', ']h', '<cmd>lua require"gitsigns".next_hunk()<CR>', {})
        end
    },
}
