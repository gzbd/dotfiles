return {
    -- "kyazdani42/blue-moon",
    dir = "~/Repositories/Private/blue-moon",
    lazy = false,    -- main colorscheme should always be loaded
    priority = 1000, -- make sure to load this before all the other start plugins
    config = function()
        vim.opt.termguicolors = true
        vim.cmd([[colorscheme blue-moon]])
    end
}
