return {
    "junegunn/vim-easy-align",
    lazy = false,
    keys = {
        { "ga", "<Plug>(EasyAlign)", mode = "x", desc = "Start interactive EasyAlign in visual mode (e.g. vipga)" },
        {
            "ga",
            "<Plug>(EasyAlign)",
            mode = "n",
            desc = "Start interactive EasyAlign for a motion/text object (e.g. gaip)"
        },
    }
}
