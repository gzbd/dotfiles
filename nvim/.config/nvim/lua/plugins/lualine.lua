local filename = {
    'filename',
    color = 'Normal',
    path = 1,
    symbols = {
        modified = '%#DiagnosticSignWarn#',
        readonly = '%#Error#',
        unnamed = '[No Name]',
        newfile = '[New]',
    }
}

local tabs = {
    'tabs',
    mode = 2,
    max_length = vim.o.columns * 2,
}


-- Used to extend the signcolumn background in the winbar.
local function signcolumn_fill()
    -- if these options are not set, nothing to do, return early
    if vim.o.signcolumn == 'no' and vim.o.number == false then
        return
    end

    local signs = vim.fn.sign_getplaced(vim.api.nvim_buf_get_number(0), { group = '*' })
    local line_signs = {}
    for _, s in ipairs(signs[1]['signs']) do
        if line_signs[s['lnum']] == nil or line_signs[s['lnum']] == 0 then
            line_signs[s['lnum']] = 1
        else
            line_signs[s['lnum']] = line_signs[s['lnum']] + 1
        end
    end

    local highest_sign_number = 0
    for _, val in pairs(line_signs) do
        if highest_sign_number < val then
            highest_sign_number = val
        end
    end
    -- crop sign number to max from signcolumn setting value
    local signcolumn = highest_sign_number
    if signcolumn > 2 then
        signcolumn = 2
    end

    local buflines = 0
    if vim.o.number ~= false then
        buflines = string.len(tostring(vim.api.nvim_buf_line_count(0)))
        -- respect numberwidth
        if buflines < 4 then
            buflines = 4
        end
    end

    -- sign takes space of two chars
    local offset = 2 * signcolumn + buflines - 1
    if offset <= 0 then
        return
    end

    local leftpad = string.rep(" ", offset)
    return "%#SignColumn#" .. leftpad .. "%*"
end

vim.api.nvim_create_autocmd("BufNew", {
    pattern = "*",
    callback = function()
        local cwd = vim.fn.getcwd()
        local tabname = vim.fn.fnamemodify(cwd, ":t")
        local tabnr = vim.api.nvim_win_get_tabpage(0)

        vim.api.nvim_tabpage_set_var(tabnr, "tabname", tabname)
    end
})


-- blue-moon theme
local colors = {
    color0 = "#1b1e2b",
    color1 = "#d0e7d0",
    color2 = "#292d3e",
    color3 = "#697098",
    color4 = "#959dcb",
    color5 = "#89bbdd",
    color6 = "#a9a3db",
}

local bluemoon = {
    replace = {
        a = { fg = colors.color0, bg = colors.color1, gui = "bold", },
        b = { fg = colors.color1, bg = colors.color2 },
        c = { fg = colors.color3, bg = colors.color0 },
    },
    insert = {
        a = { fg = colors.color0, bg = colors.color4, gui = "bold", },
        b = { fg = colors.color4, bg = colors.color2 },
        c = { fg = colors.color3, bg = colors.color0 },
    },
    visual = {
        a = { fg = colors.color0, bg = colors.color5, gui = "bold", },
        b = { fg = colors.color5, bg = colors.color2 },
        c = { fg = colors.color3, bg = colors.color0 },
    },
    normal = {
        a = { fg = colors.color0, bg = colors.color6, gui = "bold", },
        b = { fg = colors.color6, bg = colors.color2 },
        c = { fg = colors.color3, bg = colors.color0 },
    },
    inactive = {
        a = { fg = colors.color3, bg = colors.color0, gui = "bold", },
        b = { fg = colors.color3, bg = colors.color0 },
        c = { fg = colors.color3, bg = colors.color0 },
    },
}


return {
    "gzbd/lualine.nvim",
    dependencies = {
        {
            "SmiteshP/nvim-navic",
            opts = {
                icons = {
                    Text = " ",
                    String = " ",
                    Array = " ",
                    Object = "󰅩 ",
                    Namespace = " ",
                    Method = "𝕞 ",
                    Function = "󰊕 ",
                    Constructor = " ",
                    Field = " ",
                    Variable = "󰫧 ",
                    Class = " ",
                    Interface = " ",
                    Module = " ",
                    Property = " ",
                    Unit = " ",
                    Value = " ",
                    Enum = " ",
                    Keyword = " ",
                    Snippet = " ",
                    Color = " ",
                    File = " ",
                    Reference = " ",
                    Folder = " ",
                    EnumMember = " ",
                    Constant = " ",
                    Struct = " ",
                    Event = " ",
                    Operator = " ",
                    TypeParameter = " ",
                    Copilot = "🤖 ",
                    Boolean = " ",
                },
                lsp = { auto_attach = true },
            }
        },
    },
    branch = "feat/commit-component",
    lazy = false,
    opts = function()
        return {
            options = {
                icons_enabled = true,
                theme = bluemoon,
                component_separators = '',
                section_separators = '',
                disabled_filetypes = {
                    statusline = {},
                    winbar = { 'no-neck-pain' },
                },
                ignore_focus = {},
                globalstatus = true,
                refresh = {
                    statusline = 500,
                    tabline = 500,
                    winbar = 500,
                }
            },
            sections = {
                lualine_a = { 'mode' },
                lualine_b = {
                    'diff',
                    {
                        'diagnostics',
                        diagnostics_color = {
                            error = 'DiagnosticSignError',
                            warn  = 'DiagnosticSignWarn',
                            info  = 'DiagnosticSignInfo',
                            hint  = 'DiagnosticSignHint'
                        },
                        symbols = { error = ' ', warn = ' ', info = ' ', hint = ' ' },
                    },
                },
                lualine_c = {
                    'navic',
                },
                lualine_x = {},
                lualine_y = { 'location', 'progress' },
                lualine_z = { 'encoding', 'fileformat', 'filesize' }
            },
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = {},
                lualine_x = {},
                lualine_y = {},
                lualine_z = {}
            },
            tabline = {
                lualine_a = { function()
                    local session = require("possession.session").get_session_name()
                    if not session then
                        return ''
                    end

                    return "󱎫  " .. session
                end },
                lualine_b = { 'branch' },
                lualine_c = { {
                    'commit',
                    diff_against_master = true,
                    findout_master_name = true,
                } },
                lualine_x = {},
                lualine_y = {},
                lualine_z = { tabs }
            },
            winbar = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = { '%=', { 'filetype', icon_only = true }, filename, '%=' },
                lualine_x = {},
                lualine_y = {},
                lualine_z = {}
            },
            inactive_winbar = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = { '%=', { 'filetype', icon_only = true }, filename, '%=' },
                lualine_x = {},
                lualine_y = {},
                lualine_z = {}
            },
            extensions = {}
        }
    end
}
