return {
    "sindrets/diffview.nvim",
    lazy = false,
    opts = {
        enhanced_diff_hl = true,
        signs = {
          fold_closed = "",
          fold_open = "",
          done = "",
        },
        hooks = {
          diff_buf_win_enter = function(bufnr)
            vim.opt_local.foldcolumn = "0"
          end,
        }
    },
    keys = {
        { "<leader>dvc", ":DiffviewClose<CR>",       desc = "Close diff view" },
        { "<leader>dvm", ":DiffviewOpen master<CR>", desc = "Open diff view against master" },
        { "<leader>dvs", ":DiffviewOpen<CR>",        desc = "Open diff view against staging" },
    },
}
