return {
    "cocopon/iceberg.vim",
    lazy = false,    -- main colorscheme should always be loaded
    priority = 1000, -- make sure to load this before all the other start plugins
    enabled = false,
    config = function()
        vim.opt.termguicolors = true
        vim.cmd([[colorscheme iceberg]])

        vim.cmd([[hi link NormalFloat Normal]])
        vim.cmd([[hi link FloatBorder SpecialKey]])
        vim.cmd([[hi link LspSignatureActiveParameter Search]])

        -- Make function definitions highlighted in orange. This requires changing
        -- @function hl group that catches all function definition/calls and resetting
        -- back function calls highlights.
        vim.cmd([[hi link @function vimFunction]])
        vim.cmd([[hi link @function.call TSFunction]])
        vim.cmd([[hi link @method vimFunction]])
        vim.cmd([[hi link @method.call TSFunction]])

        -- Underline the spelling errors
        vim.cmd([[hi SpellBad gui=underline]])
        vim.cmd([[hi SpellCap gui=underline]])
        vim.cmd([[hi SpellRare gui=underline]])
        vim.cmd([[hi SpellLocal gui=underline]])

        -- Hide all semantic highlight groups for now, util resolved by Iceberg upstream
        for _, group in ipairs(vim.fn.getcompletion("@lsp", "highlight")) do
            vim.api.nvim_set_hl(0, group, {})
        end

        vim.cmd([[hi VertSplit ctermbg=NONE guibg=NONE guifg=#1e2132]])
        vim.cmd([[hi StatusLineNC guifg=#161821]])
        vim.cmd([[hi StatusLine guifg=#161821]])
        vim.cmd([[hi Search guibg=#3e445e guifg=#c6c8d1]])

        local get_color = function(hlname, surface, ui)
            return vim.fn.synIDattr(vim.fn.hlID(hlname), surface, ui)
        end

        local cterm_signcolumnbg = get_color("SignColumn", "bg", "cterm")
        local gui_signcolumnbg = get_color("SignColumn", "bg", "gui")
        local cterm_cursorlinenrbg = get_color("CursorLineNr", "bg", "cterm")
        local gui_cursorlinenrbg = get_color('CursorLineNr', 'bg', 'gui')

        vim.cmd("hi Pmenu ctermbg=" .. cterm_signcolumnbg .. " guibg=" .. gui_signcolumnbg)
        vim.cmd("hi PmenuSel ctermbg=" .. cterm_cursorlinenrbg .. " guibg=" .. gui_cursorlinenrbg)
        vim.cmd("hi NvimTreeNormal ctermbg=" .. cterm_signcolumnbg .. " guibg=" .. gui_signcolumnbg)
        vim.cmd("hi NvimTreeStatusLine ctermbg=" ..
            cterm_signcolumnbg ..
            " guibg=" .. gui_signcolumnbg .. " ctermfg=" .. cterm_signcolumnbg .. " guifg=" .. gui_signcolumnbg)
        vim.cmd("hi NvimTreeStatusLineNC ctermbg=" ..
            cterm_signcolumnbg ..
            " guibg=" .. gui_signcolumnbg .. " ctermfg=" .. cterm_signcolumnbg .. " guifg=" .. gui_signcolumnbg)
        vim.cmd("hi NvimTreeVertSplit ctermbg=" ..
            cterm_signcolumnbg ..
            " guibg=" .. gui_signcolumnbg .. " ctermfg=" .. cterm_signcolumnbg .. " guifg=" .. gui_signcolumnbg)
    end,
}
