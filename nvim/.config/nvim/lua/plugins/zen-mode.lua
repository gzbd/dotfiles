return {
    "folke/zen-mode.nvim",
    lazy = false,
    opts = {
        window = {
            backdrop = 1,
        },
        plugins = {
            twilight = { enabled = false },
        },
    },
    keys = {
        { "<leader>zm", "<cmd>ZenMode<CR>", desc = "Toggle ZenMode" }
    }
}
