return {
    "scrooloose/nerdcommenter",
    lazy = false,
    config = function()
        vim.g.NERDSpaceDelims = 1
    end
}
