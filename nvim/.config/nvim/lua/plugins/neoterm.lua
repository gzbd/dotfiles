return {
    "kassio/neoterm",
    config = function()
        vim.g.neoterm_default_mod = 'botright'
    end
}
