return {
    "kristijanhusak/vim-dadbod-ui",
    dependencies = {
        "tpope/vim-dadbod",
        {
            "kristijanhusak/vim-dadbod-completion",
            config = function()
                vim.g.vim_dadbod_completion_mark = ''
                vim.g.db_ui_use_nerd_fonts = 1

                local augroup = vim.api.nvim_create_augroup
                local autocmd = vim.api.nvim_create_autocmd

                local group = augroup('dadbod-completion', {})
                autocmd("FileType", {
                    pattern = { "sql", "mysql", "plsql" },
                    group = group,
                    callback = function()
                        require('cmp').setup.buffer({
                            sources = { { name = 'vim-dadbod-completion' } } })
                    end,
                })
            end
        }

    },
    lazy = false,
}
