return {
    "tpope/vim-fugitive",
    lazy = false,
    config = function()
        local augroup = vim.api.nvim_create_augroup
        local autocmd = vim.api.nvim_create_autocmd

        local group = augroup('myfugitive', {})

        autocmd({ "BufRead", "BufNewFile" }, {
            pattern = "fugitive://*",
            group = group,
            callback = function()
                vim.opt_local.foldmethod = "syntax"
            end,
        })
        autocmd("FileType", {
            pattern = "git",
            group = group,
            callback = function()
                vim.opt_local.foldmethod = "syntax"
            end,
        })
    end,
    keys = {
        {
            "<leader>gt",
            ":tabnew <bar> G <bar> only<CR>",
            desc = "Open git status in a new tab"
        },
        {
            "<leader>gs",
            function()
                local function showFugitiveGit()
                    if vim.fn.FugitiveHead() ~= '' then
                        vim.cmd [[
                            Git
                            vertical resize 40
                            setlocal foldlevel=1
                            ]]
                    end
                end

                if vim.fn.buflisted(vim.fn.bufname('fugitive:///*/.git//$')) ~= 0 then
                    vim.cmd [[ execute ":bdelete" bufname('fugitive:///*/.git//$') ]]
                else
                    showFugitiveGit()
                end
            end,
            desc = "Toggle git status"
        },
        {
            "<leader>gh",
            "<cmd>Flog -path=%<CR>",
            desc = "Show file git log"
        },
        {
            "<leader>gl",
            "<cmd>Flog<CR>",
            desc = "Show git log",
        },
        {
            "<leader>glh",
            "<cmd>exec 'Flog -- -s -L' . line('.') . ',+1:' . resolve(expand('%:p'))<CR>",
            desc = "Show git log for the current line"
        },
        {
            "<leader>gds",
            ":Gdiffsplit<CR>",
            desc = "Open current file in horizontal diff split",
        }
    }
}
