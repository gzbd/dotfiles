return {
    "jedrzejboczar/possession.nvim",
    dependencies = { 'nvim-lua/plenary.nvim' },
    lazy = false,
    config = function(_, opts)
        require('possession').setup(opts)
        require('telescope').load_extension('possession')
    end
}
