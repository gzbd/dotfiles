return {
    "shortcuts/no-neck-pain.nvim",
    lazy = false,
    version = "*",
    opts = {
        width = 120,
        buffers = {
            colors = {
                background = "#292d3e"
            }
        },
    },
    keys = {
        { "<leader>nnp", ":NoNeckPain<CR>", desc = "Toggle No Neck Pain" }
    }
}
