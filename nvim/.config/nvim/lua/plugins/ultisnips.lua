return {
    'SirVer/ultisnips',
    dependencies = {
        'honza/vim-snippets',
    },
    lazy = false,
    config = function()
        -- UltimateSnips mapping interferes with <c-k> signature_help mapping in insert
        -- mode. Remap UltimateSnips since I use it barely.
        vim.g.UltiSnipsJumpForwardTrigger = "<c-a>"
        vim.g.UltiSnipsJumpBackwardTrigger = "<c-b>"

        -- Without this, ultisnips makes nvim input mode laggy.
        -- https://github.com/quangnguyen30192/cmp-nvim-ultisnips/issues/91
        vim.g.UltiSnipsEnableSnipMate = false
    end
}
