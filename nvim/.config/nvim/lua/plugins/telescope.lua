return {
    "nvim-telescope/telescope.nvim",
    dependencies = {
        "nvim-lua/plenary.nvim",
        "FollieHiyuki/telescope-symbols.nvim",
        "nvim-tree/nvim-web-devicons",
        "nvim-telescope/telescope-file-browser.nvim",
        "nvim-telescope/telescope-ui-select.nvim"
    },
    lazy = false,
    opts = function()
        local fb_actions = require "telescope".extensions.file_browser.actions
        local actions = require "telescope.actions"
        return {
            defaults = {
                initial_mode = 'normal',
                vimgrep_arguments = {
                    'rg', '--color=never', '--no-heading', '--with-filename',
                    '--line-number', '--column', '--smart-case', '--iglob', '!.git',
                    '--hidden'
                },
                sorting_strategy = "ascending",
                layout_config = {
                    horizontal = {
                        width = { 0.75, max = 230 },
                        preview_cutoff = 180,
                        preview_width = 0.5,
                    },
                    prompt_position = 'top'
                },
                selection_strategy = "row",
                path_display = {
                    truncate = 1,
                },
                mappings = {
                    n = {
                        ["<C-c>"] = actions.close,
                    }
                },
            },
            pickers = {
                lsp_references = {
                    fname_width = 200, -- take all the space
                },
                lsp_implementations = {
                    fname_width = 200, -- take all the space
                },
            },
            extensions = {
                ["ui-select"] = { layout_strategy = "center" },
                file_browser = {
                    grouped = true,
                    hijack_netrw = true,
                    hidden = true,
                    respect_gitignore = false,
                    display_stat = { size = true, date = true, },
                    dir_icon = "",
                    dir_icon_hl = "@keyword.operator",
                    git_status = false,
                    follow_symlinks = true,
                    mappings = {
                        ["n"] = {
                            ["h"] = fb_actions.goto_parent_dir,
                            ["l"] = fb_actions.change_cwd,
                            ["<leader>fc"] = fb_actions.create,
                            ["<leader>fr"] = fb_actions.rename,
                            ["<leader>fm"] = fb_actions.move,
                            ["<leader>fy"] = fb_actions.copy,
                            ["<leader>fd"] = fb_actions.remove,
                        },
                    },
                },
            },
        }
    end,
    config = function(_, opts)
        local telescope = require("telescope")
        telescope.setup(opts)

        telescope.load_extension("file_browser")
        telescope.load_extension("ui-select")

        vim.cmd([[hi! link TelescopeBorder FloatBorder]])
        vim.cmd([[hi! link TelescopePromptTitle Label]])
        vim.cmd([[hi! link TelescopePromptPrefix Label]])
        vim.cmd([[hi! link TelescopePromptCounter Label]])
        vim.cmd([[hi! link TelescopeResultsTitle Label]])
        vim.cmd([[hi! link TelescopePreviewTitle Label]])
        vim.cmd([[hi! link TelescopePreviewDirectory @keyword.operator]])
        vim.cmd([[hi! link TelescopeMatching @keyword.operator]])
    end,
    keys = {
        {
            "<C-f>",
            function()
                require("telescope.builtin").find_files {
                    find_command = { 'rg', '--files', '--iglob', '!.git', '--hidden' }
                }
            end,
            desc = "Search for files in project"
        },
        { "<C-a>",      "<cmd>Telescope oldfiles<cr>",                                               desc = "Search files history" },
        {
            "<C-s>",
            "<cmd>Telescope grep_string<cr>",
            desc =
            "Grep project files for word under cursor"
        },
        { "<C-g>",      "<cmd>Telescope live_grep<cr>",                                              desc = "Grep project files" },
        { "gr",         "<cmd>Telescope lsp_references<cr>",                                         desc = "Find LSP references" },
        { "gd",         "<cmd>Telescope lsp_definitions<cr>",                                        desc = "Find LSP definitions" },
        { "gi",         "<cmd>Telescope lsp_implementations<cr>",                                    desc = "Find LSP implementations" },
        { "gds",        function() require("telescope-pretty-symbols").prettyDocumentSymbols() end,  desc = "Find LSP document symbols" },
        { "gws",        function() require("telescope-pretty-symbols").prettyWorkspaceSymbols() end, desc = "Find LSP workspace symbols" },
        { "<leader>td", "<cmd>Telescope diagnostics<cr>",                                            desc = "Show diagnostics in project" },
        {
            "<leader>rl",
            function()
                local make_entry = require 'telescope.make_entry'
                require("telescope.builtin").find_files {
                    prompt_title = 'Find repositories',
                    find_command = {
                        'fd', '--min-depth', '1', '--max-depth', '4', '-t', 'directory',
                        '--no-ignore', '--hidden', '--glob', '.git', vim.fn.expand('$HOME/Repositories'), '-x',
                        'dirname', '{}'
                    },
                    previewer = require("telescope.config").values.file_previewer({}),
                    entry_maker = function(entry)
                        local gen_from_file = make_entry.gen_from_file({
                            cwd = vim.fn.expand('$HOME') })
                        local res = gen_from_file(entry)
                        local display = function(old_display)
                            return function(entry)
                                local d, hl_group = old_display({
                                    value = entry.value ..
                                        '/.git' -- readd scm dir to properly display icon
                                })
                                d = string.sub(d, 1, #d - 5)
                                return d, hl_group
                            end
                        end
                        res.display = display(res.display)
                        return res
                    end
                }
            end,
            desc = "Find for repositories"
        },
        {
            "<leader>dl",
            function()
                local pickers = require "telescope.pickers"
                local finders = require "telescope.finders"
                local conf = require("telescope.config").values
                local actions = require "telescope.actions"
                local action_state = require "telescope.actions.state"
                local Job = require 'plenary.job'
                local rds = function(opts)
                    opts = opts or {}
                    pickers.new(opts, {
                        prompt_title = "Brainly databases",
                        sorter = conf.generic_sorter(opts),
                        finder = finders.new_oneshot_job(
                            { "/home/gzbd/.bin/rds-access-psql.sh", "list" },
                            {
                                entry_maker = function(entry)
                                    return {
                                        value = entry,
                                        display = " " .. entry,
                                        ordinal = entry,
                                    }
                                end
                            }),
                        attach_mappings = function(prompt_bufnr, map)
                            actions.select_default:replace(function()
                                actions.close(prompt_bufnr)
                                local dbname = action_state.get_selected_entry()

                                Job:new({
                                    command = "/home/gzbd/.bin/rds-access-psql.sh",
                                    args = { "get", dbname.value, "ro" },
                                    on_exit = function(res, return_val)
                                        if return_val < 0 then
                                            print("failed to get rds credentials for " .. dbname.value)
                                            return
                                        end

                                        local creds = res:result()[1]
                                        local dbs = vim.g.dbs
                                        if dbs == nil then
                                            dbs = {}
                                        end
                                        dbs[dbname.value] = creds
                                        vim.g.dbs = dbs
                                        print("Connection " .. dbname.value .. " is set!")
                                    end
                                }):start()
                            end)
                            return true
                        end,
                    }):find()
                end

                -- to execute the function
                -- rds()
                rds(require("telescope.themes").get_dropdown({}))
            end,
            desc = "Connect to the RDS database",
        },
        { "<leader>tc", "<cmd>Telescope commands<cr>",                desc = "Find commands" },
        { "<leader>tm", "<cmd>Telescope keymaps<cr>",                 desc = "Find keymaps" },
        { "<leader>th", "<cmd>Telescope help_tags<cr>",               desc = "Find help tags" },
        { "<leader>tb", "<cmd>Telescope file_browser path=%:p:h<cr>", desc = "Open file browser" },
        { "<leader>tr", "<cmd>Telescope resume<cr>",                  desc = "Open last Telescope window" },
        { "<leader>gb", "<cmd>Telescope git_branches<cr>",            desc = "Find git branches" },
        { "<leader>sl", "<cmd>Telescope possession list<cr>",         desc = "List sessions" }
    }
}
