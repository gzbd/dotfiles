return {
    "neovim/nvim-lspconfig",
    dependencies = {
        { "folke/neodev.nvim", opts = {} },
    },
    setup = {},
    config = function(_, opts)
        -- Diangostics setup
        vim.diagnostic.config({
            virtual_text = false,
            underline = false,
            float = {
                border = 'single',
                focusable = false,
                source = true,
                max_width = 75,
                max_height = 15,
            },
        })

        vim.fn.sign_define("DiagnosticSignError", { text = "", texthl = "DiagnosticSignError" })
        -- vim.cmd([[hi! link DiagnosticSignError SyntasticStyleErrorSign]])

        vim.fn.sign_define("DiagnosticSignWarn", { text = "", texthl = "DiagnosticSignWarning" })
        -- vim.cmd([[hi! link DiagnosticSignWarn SyntasticStyleWarningSign]])

        vim.fn.sign_define("DiagnosticSignInfo", { text = "", texthl = "DiagnosticSignInformation" })
        -- vim.cmd([[hi! link DiagnosticSignInfo CursorColumn]])

        vim.fn.sign_define("DiagnosticSignHint", { text = "", texthl = "DiagnosticSignHint" })
        -- vim.cmd([[hi! link DiagnosticSignHint CursorColumn]])

        -- override globaly preview window, to have borders in hover
        local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
        function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
            opts = opts or {}
            opts.border = 'rounded'
            opts.focusable = false
            opts.max_width = 75
            opts.max_height = 15
            return orig_util_open_floating_preview(contents, syntax, opts, ...)
        end

        local capabilities = require('cmp_nvim_lsp').default_capabilities()
        local lspconfig = require("lspconfig")

        lspconfig.bashls.setup({
            capabilities = capabilities,
        })

        lspconfig.dockerls.setup({
            capabilities = capabilities,
        })

        lspconfig.lua_ls.setup({
            capabilities = capabilities,
            settings = {
                Lua = {
                    runtime = { version = 'LuaJIT', path = vim.split(package.path, ';') },
                }
            },
        })

        lspconfig.sqlls.setup({
            capabilities = capabilities,
            cmd = { 'sql-language-server', 'up', '--method', 'stdio' }
        })

        lspconfig.terraform_lsp.setup({
            capabilities = capabilities,
        })

        lspconfig.ts_ls.setup({
            capabilities = capabilities,
        })

        lspconfig.vimls.setup({
            capabilities = capabilities,
        })

        lspconfig.gopls.setup({
            capabilities = capabilities,
            cmd = { 'gopls', 'serve' },
            on_attach = function(client)
                client.server_capabilities.document_formatting = false -- done by efm
            end
        })


        -- python config
        local util = require('lspconfig/util')
        local path = util.path
        local root_files = {
            'Pipfile', 'setup.py', 'pyproject.toml', 'setup.cfg', 'requirements.txt',
            '.git'
        }

        local function get_python_path(workspace)
            -- Use activated virtualenv.
            if vim.env.VIRTUAL_ENV then
                return path.join(vim.env.VIRTUAL_ENV, 'bin', 'python')
            end

            -- Find and use virtualenv from pipenv in workspace directory.
            local match = vim.fn.glob(path.join(workspace, '**/Pipfile'))
            if match ~= '' then
                local venv = vim.fn.trim(vim.fn.system(
                    'PIPENV_PIPFILE=' .. match ..
                    ' pipenv --venv'))
                return path.join(venv, 'bin', 'python')
            end

            -- Fallback to system Python.
            return vim.fn.exepath('python3') or vim.fn.exepath('python') or 'python'
        end

        lspconfig.pyright.setup {
            capabilities = capabilities,
            root_dir = function(filename)
                return util.root_pattern(unpack(root_files))(filename) or
                    path.dirname(filename)
            end,
            on_init = function(client)
                client.config.settings.python.pythonPath = get_python_path(client.config.root_dir)
            end
        }

        -- efm
        lspconfig.efm.setup {
            capabilities = capabilities,
            filetypes = {
                'vim', 'go', 'json', 'yaml', 'python', 'dockerfile', 'sh', 'terraform', 'hcl',
                'gitcommit'
            },
            init_options = { documentFormatting = true },
            settings = {
                rootMarkers = { '.git', 'go.mod', 'Pipfile', '.terraform' },
                languages = {
                    vim = { { lintCommand = 'vint', lintFormats = { '%f:%l:%c: %m' } } },
                    go = {
                        { formatCommand = "gofmt",     formatStdin = true },
                        { formatCommand = "goimports", formatStdin = true }
                    },
                    gitcommit = {
                        {
                            lintCommand =
                            'gitlint --contrib=contrib-title-conventional-commits',
                            lintStdin = true,
                            lintFormats = { '%l: %m' }
                        }
                    },
                    json = {
                        {
                            formatCommand = 'prettier --tab-width=2 --parser json',
                            formatStdin = true
                        }, {
                        lintCommand = 'jq .',
                        lintStdin = true,
                        lintFormats = { '%m at line %l, column %c' }
                    }
                    },
                    yaml = {
                        {
                            formatCommand = 'prettier --tab-width=2 --parser yaml',
                            formatStdin = true
                        }, {
                        lintCommand = 'yamllint -f parsable -',
                        lintStdin = true,
                        lintIgnoreExitCode = true
                    }
                    },
                    python = {
                        { formatCommand = "black --fast -", formatStdin = true },
                        {
                            lintCommand =
                            "flake8 --max-line-length 88 --format '%(path)s:%(row)d:%(col)d: %(code)s %(code)s %(text)s' --stdin-display-name ${INPUT} -",
                            lintStdin = true,
                            lintIgnoreExitCode = true,
                            lintFormats = { "%f:%l:%c: %t%n%n%n %m" },
                            lintSource = "flake8"
                        }, {
                        lintCommand = "mypy --show-column-numbers --ignore-missing-imports",
                        lintFormats = {
                            "%f:%l:%c: %trror: %m", "%f:%l:%c: %tarning: %m",
                            "%f:%l:%c: %tote: %m"
                        },
                        lintSource = "mypy"
                    }
                    },
                    dockerfile = {
                        {
                            lintCommand = 'hadolint --no-color',
                            lintFormats = { '%f:%l %m' }
                        }
                    },
                    sh = {
                        {
                            lintCommand = 'shellcheck -f gcc -x',
                            lintSource = 'shellcheck',
                            lintFormats = {
                                '%f:%l:%c: %trror: %m', '%f:%l:%c: %tarning: %m',
                                '%f:%l:%c: %tote: %m'
                            }
                        }, { formatCommand = 'shfmt -ci -s -bn', formatStdin = true }
                    },
                    terraform = {
                        { formatCommand = "terraform fmt -no-color -", formatStdin = true }
                    },
                    hcl = {
                        { formatCommand = "terraform fmt -no-color -", formatStdin = true }
                    },
                }
            }
        }

        vim.api.nvim_set_keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>",
            { desc = "Go to declaration" })
        vim.api.nvim_set_keymap("n", "gdt", "<cmd>lua vim.lsp.buf.type_definition()<CR>",
            { desc = "Go to type definition" })
        vim.api.nvim_set_keymap("n", "gca", "<cmd>lua vim.lsp.buf.code_action()<CR>", { desc = "Run code action" })
        vim.api.nvim_set_keymap("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", { desc = "Hover hint" })
        vim.api.nvim_set_keymap("n", "[d", "<cmd>lua vim.diagnostic.goto_prev()<CR>",
            { desc = "Go to previous diagnostic item" })
        vim.api.nvim_set_keymap("n", "]d", "<cmd>lua vim.diagnostic.goto_next()<CR>",
            { desc = "Go to next diagnostic item" })
        vim.api.nvim_set_keymap("n", "<leader>k", "<cmd>lua vim.lsp.buf.signature_help()<CR>",
            { desc = "Show signature" })
        vim.api.nvim_set_keymap("i", "<c-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>",
            { desc = "Show signature" })
        vim.api.nvim_set_keymap("n", "<leader>rn", "<cmd>lua vim.lsp.buf.rename()<CR>",
            { desc = "Rename symbol" })
        vim.api.nvim_set_keymap("n", "<leader>f", "<cmd>lua vim.lsp.buf.format()<CR>",
            { desc = "Auto-format buffer" })
        vim.api.nvim_set_keymap("n", "<leader>do", "<cmd>lua vim.diagnostic.open_float()<CR>",
            { desc = "Open diagnostic flow window" })
        vim.api.nvim_set_keymap("n", "<leader>ds", "<cmd>lua vim.lsp.buf.document_highlight()<CR>",
            { desc = "Highlight symbol" })
        vim.api.nvim_set_keymap("n", "<leader>dc", "<cmd>lua vim.lsp.buf.clear_references<CR>",
            { desc = "Clear highlight symbol" })

        local augroup = vim.api.nvim_create_augroup
        local autocmd = vim.api.nvim_create_autocmd

        augroup("auto-formatting", {})
        autocmd("BufWritePre", {
            desc = "Format file according to standards",
            group = "auto-formatting",
            pattern = {
                "*.go", "go.mod",
                "*.py",
                "*.lua",
                "*.ts", "*.tsx", "*.js", "*.jsx",
                "*.json", "*.yml", "*.yaml",
                "*Dockerfile", "*-Dockerfile",
                "*.sh",
                "*.tf", "*.hcl",
            },
            command = [[lua vim.lsp.buf.format({async = false })]]
        })
    end,
}
