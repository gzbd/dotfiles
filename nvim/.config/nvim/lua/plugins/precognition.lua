return {
    "tris203/precognition.nvim",
    lazy = false,
    opts = {
        startVisible = false,
        highlightColor = { link = "Comment" },
    },
    keys = {
        { "<leader>pt", ":Precognition toggle<CR>", desc = "Toggle motion visual hints" }
    }
}
