return {
    "vim-test/vim-test",
    config = function()
        vim.cmd([[let test#python#runner = 'pyunit']])
        vim.cmd([[let test#strategy = 'neovim']])
        vim.cmd([[let test#neovim#start_normal = 1]])
    end,
    keys = {
        { "<leader>tn", "<cmd>TestNearest<CR>", desc = "Run test for nearest function" },
        { "<leader>tf", "<cmd>TestFile<CR>",    desc = "Run test for whole file" },
        { "<leader>ts", "<cmd>TestSuite<CR>",   desc = "Run test for whole suite" },
        { "<leader>tl", "<cmd>TestLast<CR>",    desc = "Repeat last test" },
        { "<leader>tv", "<cmd>TestVisit<CR>",   desc = "Visit last test file" },
    }
}
