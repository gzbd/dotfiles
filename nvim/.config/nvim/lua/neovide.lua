vim.o.guifont = "JetBrainsMono Nerd Font For Neovide:h11.0"
vim.o.linespace = -2


vim.g.neovide_refresh_rate = 144
vim.g.neovide_cursor_animation_length = 0.02
vim.g.neovide_scroll_animation_length = 0.2

vim.g.neovide_padding_top = 20
vim.g.neovide_padding_bottom = 20
vim.g.neovide_padding_left = 20
vim.g.neovide_padding_right = 20

vim.g.neovide_underline_stroke_scale = 1.2
vim.g.neovide_floating_shadow = false

-- Scale fonts
vim.g.neovide_scale_factor = 1.0
local change_scale_factor = function(delta)
    vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
end
vim.keymap.set("n", "<C-=>", function() change_scale_factor(1.10) end, { desc = "Increase font size by 10%" })
vim.keymap.set("n", "<C-->", function() change_scale_factor(1 / 1.10) end, { desc = "Decrease font size by 10%" })
vim.keymap.set("n", "<C-0>", function() vim.g.neovide_scale_factor = 1.0 end, { desc = "Reset the font size" })

vim.g.neovide_floating_shadow = false
vim.g.neovide_floating_z_height = 0
vim.g.neovide_light_angle_degrees = 0
vim.g.neovide_light_radius = 0
