-- Copied from https://github.com/nvim-telescope/telescope.nvim/issues/2014
local telescopeUtilities = require('telescope.utils')
local telescopeMakeEntryModule = require('telescope.make_entry')
local telescopeEntryDisplayModule = require('telescope.pickers.entry_display')

local kind_icons = {
    Text = "",
    String = "",
    Array = "",
    Object = "󰅩",
    Namespace = "",
    Method = "𝕞",
    Function = "󰊕",
    Constructor = "",
    Field = "",
    Variable = "󰫧",
    Class = "",
    Interface = "",
    Module = "",
    Property = "",
    Unit = "",
    Value = "",
    Enum = "",
    Keyword = "",
    Snippet = "",
    Color = "",
    File = "",
    Reference = "",
    Folder = "",
    EnumMember = "",
    Constant = "",
    Struct = "",
    Event = "",
    Operator = "",
    TypeParameter = "",
    Copilot = "🤖",
    Boolean = "",
}

local telescopePickers = {}

-- Gets the File Path and its Tail (the file name) as a Tuple
function telescopePickers.getPathAndTail(fileName)
    -- Get the Tail
    local bufferNameTail = telescopeUtilities.path_tail(fileName)

    -- Now remove the tail from the Full Path
    local pathWithoutTail = require('plenary.strings').truncate(fileName, #fileName - #bufferNameTail, '')

    -- Apply truncation and other pertaining modifications to the path according to Telescope path rules
    local pathToDisplay = telescopeUtilities.transform_path({
        path_display = { 'truncate' },
    }, pathWithoutTail)

    -- Return as Tuple
    return bufferNameTail, pathToDisplay
end

function telescopePickers.prettyDocumentSymbols(localOptions)
    if localOptions ~= nil and type(localOptions) ~= 'table' then
        print("Options must be a table.")
        return
    end

    local options = localOptions or {}

    local originalEntryMaker = telescopeMakeEntryModule.gen_from_lsp_symbols(options)

    options.entry_maker = function(line)
        local originalEntryTable = originalEntryMaker(line)

        local displayer = telescopeEntryDisplayModule.create({
            separator = ' ',
            items = {
                { width = nil },
                { width = nil },
                { remaining = true },
            },
        })

        originalEntryTable.display = function(entry)
            return displayer {
                { string.format("%s", kind_icons[(entry.symbol_type:lower():gsub("^%l", string.upper))]) .. ' ' .. entry.symbol_type:lower(), 'TelescopeResultsFunction' },
                { entry.symbol_name },
                { entry.lnum .. ':' .. entry.col,                                                                                             'TelescopeResultsComment' },
            }
        end

        return originalEntryTable
    end

    require('telescope.builtin').lsp_document_symbols(options)
end

function telescopePickers.prettyWorkspaceSymbols(localOptions)
    if localOptions ~= nil and type(localOptions) ~= "table" then
        print "Options must be a table."
        return
    end

    local options = localOptions or {}

    local originalEntryMaker = telescopeMakeEntryModule.gen_from_lsp_symbols(options)

    options.entry_maker = function(line)
        local originalEntryTable = originalEntryMaker(line)

        local displayer = telescopeEntryDisplayModule.create {
            separator = " ",
            items = {
                { width = nil },
                { width = nil },
                { width = nil },
                { remaining = true },
            },
        }

        originalEntryTable.display = function(entry)
            local pathToDisplay = telescopeUtilities.transform_path({
                path_display = { shorten = { num = 2, exclude = { -2, -1 } }, "truncate" },
            }, entry.value.filename)

            return displayer {
                { string.format("%s", kind_icons[(entry.symbol_type:lower():gsub("^%l", string.upper))]) .. ' ' .. entry.symbol_type:lower(), 'TelescopeResultsFunction' },
                { entry.symbol_name },
                { pathToDisplay,                                                                                                              "TelescopeResultsComment" },
                { entry.lnum .. ':' .. entry.col,                                                                                             'TelescopeResultsComment' },
            }
        end

        return originalEntryTable
    end

    require("telescope.builtin").lsp_dynamic_workspace_symbols(options)
end

return telescopePickers
