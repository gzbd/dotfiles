local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

augroup("general", {})
autocmd("BufWritePre", {
    desc = "Clear trailing whitespaces",
    group = "general",
    pattern = "*",
    command = [[%s/\s\+$//e]]
})
autocmd({ "BufRead", "BufNewFile" }, {
    desc = "Keep folding enabled but all folds open by default",
    group = "general",
    command = "normal zR"
})

augroup("dockerfile", {})
autocmd({ "BufNewFile", "BufRead" }, {
    desc = "Set filetype to Dockerfile on files with Dockerfile prefix",
    group = "dockerfile",
    pattern = "Dockerfile*",
    command = "set filetype=dockerfile",
})

augroup("mail", {})
autocmd({ "BufNewFile", "BufRead" }, {
    desc = "Turn on spell=en_us",
    group = "mail",
    pattern = "mail",
    command = "set spell spelllang=en_us",
})
autocmd("FileType", {
    desc = "Auto format paragraphs",
    group = "mail",
    pattern = "mail",
    command = "setlocal formatoptions+=aw",
})

augroup("git", {})
autocmd({ "BufNewFile", "BufRead" }, {
    desc = "Turn on spell=en_us",
    group = "git",
    pattern = "COMMIT_EDITMSG",
    command = "set spell spelllang=en_us",
})

augroup("text-width-and-indent", {})
autocmd("FileType", {
    desc = "Defualt text width and indent options",
    group = "text-width-and-indent",
    pattern = {
        "c", "sh",
        "dockerfile", "make",
        "go", "lua",
        "javascript", "typescript", "typescriptreact", "css", "scss", "html", "htmldjango",
        "xml", "yaml", "yml", "json",
        "markdown", "bazgranina"
    },
    command = [[set tw=120 sw=4]],
})

autocmd("FileType", {
    desc = "Text width and indent options for sql",
    group = "text-width-and-indent",
    pattern = { "sql", "plsql", "mysql", "query" },
    command = [[set tw=240 sw=4]],
})
