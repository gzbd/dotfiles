local pickers = require "telescope.pickers"
local finders = require "telescope.finders"
local conf = require("telescope.config").values
local actions = require "telescope.actions"
local action_state = require "telescope.actions.state"
local Job = require'plenary.job'

local rds = function(opts)
  opts = opts or {}
  pickers.new(opts, {
    prompt_title = "rds",
    sorter = conf.generic_sorter(opts),
    finder = finders.new_oneshot_job(
      { "/home/gzbd/.bin/rds-access-psql.sh", "list" },
      {
          entry_maker = function(entry)
              return {
                  value = entry,
                  display = " " .. entry,
                  ordinal = entry,
              }
          end
      }),
    attach_mappings = function(prompt_bufnr, map)
      actions.select_default:replace(function()
        actions.close(prompt_bufnr)
        local dbname = action_state.get_selected_entry()

        Job:new({
            command = "/home/gzbd/.bin/rds-access-psql.sh",
            args = {"get", dbname.value, "ro"},
            on_exit = function(res, return_val)
                if return_val < 0 then
                    print("failed to get rds credentials for " .. dbname.value)
                    return
                end

                local creds = res:result()[1]
                vim.g.dbs = {
                  [dbname.value] = creds,
                }
            end
        }):start()

      end)
      return true
    end,
  }):find()
end

-- to execute the function
-- rds()
rds(require("telescope.themes").get_dropdown({}))
