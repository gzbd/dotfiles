nmap <leader>gt :tabnew <bar> G <bar> only<CR>
nmap <leader>gh :Flog -path=%<CR>
nmap <leader>gl :Flog<CR>
nmap <leader>glh :exec 'Floggit log -L' . line('.') . ',+1:%'<CR>
nmap <leader>gds :Gdiffsplit<CR>

augroup myfugitive
    " This makes inline diffs loaded in commit view to be folded. It makes clear
    " view which files were edited in the commit, and can jump to diff with "O".
    au BufRead,BufNewFile fugitive://* setlocal foldmethod=syntax
    au Filetype git setlocal foldmethod=syntax
augroup END

function! MyGitDiff(commit1, commit2)
    if empty(a:commit1)
        exe 'tab Git diff HEAD'
        let b:commit1 = 'HEAD'
        let b:commit2 = '.'
    elseif empty(a:commit2)
        exe 'tab Git diff ' . shellescape(a:commit1)
        let b:commit1 = a:commit1
        let b:commit2 = '.'
    else
        exe 'tab Git diff ' . shellescape(a:commit1) . ' ' . shellescape(a:commit2)
        let b:commit1 = a:commit1
        let b:commit2 = a:commit2
    endif

    no <buffer> O :call MyGitDiffShow('Gtabedit' ,b:commit1, b:commit2, GetPathFromDiffHeader(getline('.')))<cr>
    no <buffer> o :call MyGitDiffShow('Gsplit', b:commit1, b:commit2, GetPathFromDiffHeader(getline('.')))<cr>
endfunction

function! GetPathFromDiffHeader(header)
    return substitute(split(a:header, " ")[2], "a/", "", "")
endfunction

function! MyGitDiffShow(mode, commit1, commit2, path)
    exe a:mode . ' ' . fnameescape(a:commit1 . ':' . a:path)
    exe 'vert bel Gdiff ' . fnameescape(a:commit2 . ':' . a:path)
endfunction


command! -nargs=+ Gcdiff call MyGitDiff(<f-args>)
nmap <leader>gdm :tab Gcdiff master @<CR>
