local wibox = require("wibox")
local watch = require("awful.widget.watch")
local gears = require("gears")
local awful = require("awful")

local common = require("gzbd_widgets.common")

local widget = {}

local total_cpu_load_cmd = "sh -c \"paste -d' ' <(grep '^cpu' /proc/stat) <(echo '0.0'; grep '^cpu MHz' /proc/cpuinfo | cut -f2 -d: | tr -d ' ')\""
local cpus_mem_load_cmd = "sh -c \"ps -eo '%p|%C|' -o '%mem' -o '|%a' --sort=-%cpu | head -18 | tail -n +2\""


local function worker(args)
    local timeout = args.timeout or 1
    local show_graph_in_bar = args.show_graph_in_bar or false

    local cpugraph_widget = wibox.widget {
        max_value = 100,
        background_color = args.graphbg,
        step_width = args.graphstep_width or 4,
        step_spacing = args.graphstep_spacing or 0,
        widget = wibox.widget.graph,
        color = "linear:0,0:0,20:0," .. args.graph_step3 .. ":0.3," .. args.graph_step2 .. ":0.6," .. args.graph_step1,
        visible = show_graph_in_bar,
    }

    local cpus = {}
    local cpus_grid = wibox.widget {
        layout = wibox.layout.grid.vertical,
        homogeneous = false,
        forced_num_cols = 4,
        spacing = 4,
    }
    cpus_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Name</b>",
            align = "left",
            widget = wibox.widget.textbox,
        },
        1, 1, 1, 1
    )
    cpus_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Usage</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1, 2, 1, 1
    )
    cpus_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Freq</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1, 3, 1, 1
    )
    cpus_grid:add_widget_at(
        wibox.widget {
            markup = "<b>History (2 minutes)</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1, 4, 1, 1
    )

    local cpu_widget = wibox.widget {
        wibox.widget {
            text = " n/a",
            align = "center",
            widget = wibox.widget.textbox,
        },
        {
            {
                cpugraph_widget,
                reflection = {horizontal = true},
                layout = wibox.container.mirror
            },
            margin = 2,
            right = 8,
            widget = wibox.container.margin
        },
        spacing = 4,
        layout = wibox.layout.fixed.horizontal,
        set_text = function(self, text)
            self.children[1].text = " " .. text
        end
    }

    watch(total_cpu_load_cmd, timeout, function(w, stdout)
        local i = 1
        for line in stdout:gmatch("[^\r\n]+") do
            if cpus[i] == nil then
                cpus[i] = {
                    graph_widget = wibox.widget {
                        max_value = 100,
                        forced_width = common.calculate_graph_width_for_time(120, args.graphstep_width or 2, timeout, args.graphstep_spacing or 0),
                        background_color = args.graphbg,
                        step_width = args.graphstep_width or 2,
                        step_spacing = args.graphstep_spacing or 0,
                        widget = wibox.widget.graph,
                        color = "linear:0,0:0,20:0," .. args.graph_step3 .. ":0.3," .. args.graph_step2 .. ":0.6," .. args.graph_step1,
                    },
                    usage_widget = wibox.widget {
                        text = "0.0%",
                        align = "center",
                        widget = wibox.widget.textbox,
                    },
                    freq_widget = wibox.widget {
                        text = "0.0 GHz",
                        align = "center",
                        widget = wibox.widget.textbox,
                    },
                    name_widget = wibox.widget {
                        text = "n/a",
                        align = "left",
                        widget = wibox.widget.textbox,
                    },
                }

                cpus_grid:add_widget_at(
                    cpus[i]["name_widget"],
                    i + 1, 1, 1, 1
                )
                cpus_grid:add_widget_at(
                    cpus[i]["usage_widget"],
                    i + 1, 2, 1, 1
                )
                cpus_grid:add_widget_at(
                    cpus[i]["freq_widget"],
                    i + 1, 3, 1, 1
                )
                cpus_grid:add_widget_at(
                    {
                        cpus[i]["graph_widget"],
                        reflection = {horizontal = true},
                        layout = wibox.container.mirror
                    },
                    i + 1, 4, 1, 1
                )
            end

            local name, user, nice, system, idle, iowait, irq, softirq, steal, _, _, freq =
                line:match("(%w+)%s+(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+.%d+)")

            local total = user + nice + system + idle + iowait + irq + softirq + steal

            local diff_idle = idle - tonumber(cpus[i]['idle_prev'] == nil and 0 or cpus[i]['idle_prev'])
            local diff_total = total - tonumber(cpus[i]['total_prev'] == nil and 0 or cpus[i]['total_prev'])
            local diff_usage = (1000 * (diff_total - diff_idle) / diff_total + 5) / 10
            freq = tonumber(string.format("%.2f", freq / 1000))

            cpus[i]["total_prev"] = total
            cpus[i]["idle_prev"] = idle
            cpus[i]["diff_usage"] = diff_usage
            cpus[i]["name"] = name
            cpus[i]["freq"] = freq
            cpus[i]["graph_widget"]:add_value(diff_usage)
            cpus[i]["usage_widget"].text = math.floor(diff_usage) .. "%"
            cpus[i]["name_widget"].text = name
            cpus[i]["freq_widget"].text = string.format("%.2f GHz", freq)

            i = i + 1
        end

        local freq_sum = 0.0
        for num, cpu in pairs(cpus) do
            if num > 1 then
                freq_sum = freq_sum + cpu.freq
            end
        end
        local mean_freq = freq_sum / (#cpus -1)
        cpus[1].freq = mean_freq
        local mean_freq_text = string.format("%.2f GHz", mean_freq)
        cpus[1]["freq_widget"].text = mean_freq_text


        local graph_widget = w.children[2].widget.widget
        graph_widget:add_value(cpus[1].diff_usage)
        w.text = mean_freq_text
    end, cpu_widget)

    local popup, popup_timer = common.popup_with_timer(timeout)

    cpu_widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if popup.visible then
                    popup.visible = not popup.visible
                else
                    popup:move_next_to(mouse.current_widget_geometry)
                end
            end)
        )
    )

    local process_cmds = {}
    local process_grid = wibox.widget {
        layout = wibox.layout.grid.vertical,
        forced_num_cols = 4,
        homogeneous = false,
        spacing = 4,
    }
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>PID</b>",
            align = "right",
            widget = wibox.widget.textbox,
        },
        1,1,1,1
    )
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Name</b>",
            align = "left",
            widget = wibox.widget.textbox,
        },
        1,2,1,1
    )
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>CPU</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1,3,1,1
    )
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>MEM</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1,4,1,1
    )
    for i = 2, 17 + 1, 1 do
        process_grid:add_widget_at(
            wibox.widget {
                text = "n/a",
                align = "right",
                widget = wibox.widget.textbox,
            },
            i,1,1,1
        )
        local comm_widget = wibox.widget {
            text = "n/a",
            align = "left",
            widget = wibox.widget.textbox,
        }
        process_grid:add_widget_at(
            comm_widget,
            i,2,1,1
        )
        process_grid:add_widget_at(
            wibox.widget {
                text = "n/a",
                align = "center",
                widget = wibox.widget.textbox,
            },
            i,3,1,1
        )
        process_grid:add_widget_at(
            wibox.widget {
                text = "n/a",
                align = "center",
                widget = wibox.widget.textbox,
            },
            i,4,1,1
        )

        awful.tooltip {
            objects = { process_grid:get_widgets_at(i, 2)[1] },
            mode = "mouse",
            timer_function = function()
                local text = process_cmds[i - 1]
                return text
                    :gsub('%s%-', '\n  -')
                    :gsub(':/', '\n    :/') -- java classpath uses : to separate jars
            end,
        }
    end

    popup_timer:connect_signal("timeout", function()
        awful.spawn.easy_async(cpus_mem_load_cmd, function(stdout, _, _, _)
            local i = 2 -- use order in process_grid
            for line in stdout:gmatch("[^\r\n]+") do
                local columns = common.split(line, '|')

                local pid = common.strip(columns[1])
                local cpu = common.strip(columns[2]) .. "%"
                local mem = common.strip(columns[3]) .. "%"
                local cmd = columns[4]
                local comm = common.basename(cmd)
                process_cmds[i - 1] = cmd

                process_grid:get_widgets_at(i,1)[1].text = pid
                process_grid:get_widgets_at(i,2)[1].text = comm
                process_grid:get_widgets_at(i,3)[1].text = cpu
                process_grid:get_widgets_at(i,4)[1].text = mem


                i = i + 1
            end
        end)
    end)

    popup:setup {
        {
            cpus_grid,
            process_grid,
            layout = wibox.layout.fixed.horizontal,
            spacing = 32,
            spacing_widget = {
                orientation = 'vertical',
                widget = wibox.widget.separator
            },
        },
        margins = 8,
        widget = wibox.container.margin
    }


    return cpu_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
