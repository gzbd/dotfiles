local wibox = require("wibox")
local gears = require("gears")
local strings = require("gears.string")
local split = strings.split
local awful = require("awful")
local spawn = require("awful.spawn")
local watch = require("awful.widget.watch")
local markup = require("lain.util.markup")

local function trim(s)
   return s:match'^%s*(.*%S)' or ''
end

function set_pulsemixer_volume(source, vol)
    -- TODO: throttle setting volume
    -- Slider value might change dynamically and spawn a lot of command to set
    -- the volume. Race condition might occur. Below pulsemixer call should be
    -- throttle and use only last value set in X milliseconds.
    if source == nil or vol == nil then
        return
    end
    spawn.easy_async("pulsemixer --id " .. source .. " --set-volume " .. vol, function() end)
end

local widget = {}

local function worker(args)
    local args = args or {}

    local mouse_scroll_step = args.mouse_scroll_step or 1
    local check_interval = args.check_interval or 5
    local fg = args.fg
    local bg = args.bg
    local gray = args.gray
    local bar_color = args.bar_color
    local font = args.font

    local devices_check_interval = 5

    local textbox_widget = wibox.widget{
        align  = 'center',
        valign = 'center',
        widget = wibox.widget.textbox
    }

    local __output_source = gears.object{
        enable_properties = true,
        enable_auto_signals = true,
        id = 'sink-0',
        name = 'n/a'
    }

    local __mic_source = gears.object{
        enable_properties = true,
        enable_auto_signals = true,
        id = 'source-0',
        name = 'n/a'
    }

    output_source_name_textbox_widget = wibox.widget {
        align = "right",
        widget = wibox.widget.textbox
    }

    output_source_id_textbox_widget = wibox.widget {
        widget = wibox.widget.textbox
    }

    mic_source_name_textbox_widget = wibox.widget {
        align = "right",
        widget = wibox.widget.textbox
    }

    mic_source_id_textbox_widget = wibox.widget {
        widget = wibox.widget.textbox
    }

    __output_source:connect_signal('property::name', function(_, name)
        output_source_name_textbox_widget.markup = markup.fontfg(font, fg, "<i>" .. name .. "</i>")
    end)

    __output_source:connect_signal('property::volume', function()
        textbox_widget.markup = markup.fontfg(font, fg, ' ' .. tostring(__output_source.volume) .. '%  ' .. tostring(__mic_source.volume) .. '%')
    end)

    __mic_source:connect_signal('property::name', function(_, name)
        mic_source_name_textbox_widget.markup = markup.fontfg(font, fg, "<i>" .. name .. "</i>")
    end)

    __mic_source:connect_signal('property::volume', function()
        textbox_widget.markup = markup.fontfg(font, fg, ' ' .. tostring(__output_source.volume) .. '%  ' .. tostring(__mic_source.volume) .. '%')
    end)

    watch("bash -c \"pulsemixer --list | grep Sink: | grep Default\"", devices_check_interval, function(w, stdout)
        local output, _ = stdout:gsub("\n", "")
        splited_output = split(output, ":")

        output_source = trim(split(splited_output[3], ",")[1])
        output_name = trim(split(splited_output[4], ",")[1])

        if __output_source.id ~= output_source then
           __output_source.id = output_source
           __output_source.name = output_name
        end
    end, textbox_widget)

    watch("bash -c \"pulsemixer --list | grep Source: | grep Default\"", devices_check_interval, function(w, stdout)
        local output, _ = stdout:gsub("\n", "")
        splited_output = split(output, ":")

        output_source = trim(split(splited_output[3], ",")[1])
        output_name = trim(split(splited_output[4], ",")[1])

        if __mic_source.id ~= output_source then
           __mic_source.id = output_source
           __mic_source.name = output_name
        end
    end, textbox_widget)

    local function create_slider_widget(device_type)
        local widget = wibox.widget {
            bar_shape           = gears.shape.rounded_rect,
            bar_height          = 4,
            bar_color           = bar_color,
            handle_shape        = gears.shape.circle,
            handle_width        = 16,
            widget              = wibox.widget.slider,
        }

        widget:buttons(awful.util.table.join(
            awful.button({ }, 4, function()
                local value = widget:get_value()
                local next_value = value + mouse_scroll_step
                if next_value > widget:get_maximum() then
                    next_value = widget:get_maximum()
                end
                widget:set_value(next_value)
            end),
            awful.button({ }, 5, function()
                local value = widget:get_value()
                local next_value = value - mouse_scroll_step
                if next_value < widget:get_minimum() then
                    next_value = widget:get_minimum()
                end
                widget:set_value(next_value)
            end)
        ))

        watch("bash -c \"pulsemixer -l | grep Default | grep " .. device_type .. " | cut -d',' -f5 | grep -o '[0-9]\\+'\"", check_interval, function(w, stdout)
           local value, _ = stdout:gsub("\n", "")
           if value == "" then
             return
           end

           w:set_value(tonumber(value, 10))
        end, widget)

        return widget
    end

    local volume_slider_widget = create_slider_widget("Sink")
    volume_slider_widget:connect_signal("property::value", function(w)
        __output_source.volume = w.value
        set_pulsemixer_volume(__output_source.id, __output_source.volume)
    end)
    __output_source:connect_signal('property::id', function(obj, id)
       output_source_id_textbox_widget.markup = markup.fontfg(font, gray, ' ' .. __output_source.id)
    end)

    local mic_slider_widget = create_slider_widget("Source")
    mic_slider_widget:connect_signal("property::value", function(w)
        __mic_source.volume = w.value
        set_pulsemixer_volume(__mic_source.id, __mic_source.volume)
    end)
    __mic_source:connect_signal('property::id', function(obj, id)
       mic_source_id_textbox_widget.markup = markup.fontfg(font, gray, ' ' .. __mic_source.id)
    end)

    local popup = awful.popup{
        widget = {
            {
                {
                    {
                        output_source_id_textbox_widget,
                        output_source_name_textbox_widget,
                        layout = wibox.layout.align.horizontal,
                    },
                    volume_slider_widget,
                    layout  = wibox.layout.flex.vertical
                },
                {
                    {
                        mic_source_id_textbox_widget,
                        mic_source_name_textbox_widget,
                        layout = wibox.layout.align.horizontal,
                    },
                    mic_slider_widget,
                    layout  = wibox.layout.flex.vertical
                },
                spacing        = 1,
                spacing_widget = wibox.widget.separator,
                layout = wibox.layout.flex.vertical
            },
            margins = 10,
            widget  = wibox.container.margin
        },
        ontop = true,
        visible = false,
        shape = gears.shape.rounded_rect,
        maximum_width = 500,
        maximum_height = 200,
        preferred_positions = "top",
        offset = { y = -8 },
        bg = bg,
        fg = fg,
    }
    popup:buttons(
        awful.util.table.join(
            awful.button({}, 3, function()
                if popup.visible then
                    popup.visible = not popup.visible
                end
            end)
       )
    )

    textbox_widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if popup.visible then
                    popup.visible = not popup.visible
                else
                    popup:move_next_to(mouse.current_widget_geometry)
                end
            end),
            awful.button({}, 3, function()
                if popup.visible then
                    popup.visible = not popup.visible
                end
            end)
        )
    )

    return textbox_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
