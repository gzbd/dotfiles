local wibox = require("wibox")
local gears = require("gears")
local awful = require("awful")
local spawn = require("awful.spawn")
local watch = require("awful.widget.watch")
local markup = require("lain.util.markup")

local common = require("gzbd_widgets.common")

local widget = {}

local function worker(args)
    local backlight_level = 0

    local fg = args.fg
    local bg = args.bg
    local bar_color = args.bar_color
    local font = args.font
    local device = args.device
    local check_interval = args.check_interval or 5
    local mouse_scroll_step = args.mouse_scroll_step or 1
    local icon = args.icon or " "

    local textbox_widget = wibox.widget{
        align  = 'center',
        valign = 'center',
        widget = wibox.widget.textbox
    }

    local backlight_source_textbox_widget = wibox.widget {
        markup = markup.fontfg(font, fg, "<i>" .. device .. "</i>"),
        align = "right",
        widget = wibox.widget.textbox
    }

    local slider = wibox.widget {
        bar_shape           = gears.shape.rounded_rect,
        bar_height          = 4,
        bar_color           = bar_color,
        handle_shape        = gears.shape.circle,
        handle_width        = 16,
        widget              = wibox.widget.slider,
    }

    slider:buttons(awful.util.table.join(
        awful.button({ }, 4, function()
            local value = slider:get_value()
            local next_value = value + mouse_scroll_step
            if next_value > slider:get_maximum() then
                next_value = slider:get_maximum()
            end
            slider:set_value(next_value)
        end),
        awful.button({ }, 5, function()
            local value = slider:get_value()
            local next_value = value - mouse_scroll_step
            if next_value < slider:get_minimum() then
                next_value = slider:get_minimum()
            end
            slider:set_value(next_value)
        end)
    ))


    local set_backlight = function(value)
        spawn("light -S " .. value .. " -s " .. device)
    end

    local update_textbox_func = function(value)
        textbox_widget.markup = markup.fontfg(font, fg, icon .. tostring(value) .. ' %')
    end

    slider:connect_signal("property::value", function(w)
        backlight_level = w.value
        set_backlight(backlight_level)
        update_textbox_func(backlight_level)
    end)

    local popup = awful.popup{
        widget = {
            {
                {
                    {
                        {
                            markup = markup.fontfg(font, fg, 'device:'),
                            widget = wibox.widget.textbox,
                        },
                        backlight_source_textbox_widget,
                        layout = wibox.layout.align.horizontal,
                    },
                    slider,
                    layout  = wibox.layout.flex.vertical
                },
                spacing        = 1,
                spacing_widget = wibox.widget.separator,
                layout = wibox.layout.flex.vertical
            },
            margins = 10,
            widget  = wibox.container.margin
        },
        ontop = true,
        visible = false,
        shape = gears.shape.rounded_rect,
        maximum_width = 400,
        maximum_height = 200,
        preferred_positions = "top",
        offset = { y = -8 },
        bg = bg,
        fg = fg,
    }

    popup:buttons(
        awful.util.table.join(
            awful.button({}, 3, function()
                if popup.visible then
                    popup.visible = not popup.visible
                end
            end)
       )
    )

    textbox_widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if popup.visible then
                    popup.visible = not popup.visible
                else
                    popup:move_next_to(mouse.current_widget_geometry)
                end
            end)
        )
    )

    watch("light -G -s " .. device, check_interval, function(w, stdout, _)
        local value, _ = stdout:gsub("\n", "")
        w:set_value(tonumber(common.split(value, ".")[1], 10))
    end, slider)

    return textbox_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
