local wibox = require("wibox")
local watch = require("awful.widget.watch")
local awful = require("awful")

local common = require("gzbd_widgets.common")

local widget = {}

local function worker(args)
    local watched_temps = {}

    local temps_grid = wibox.widget {
        layout = wibox.layout.grid.vertical,
        forced_num_cols = 3,
        vertical_spacing = 4,
        horizontal_spacing = 16,
        homogeneous = false,
    }
    temps_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Name</b>",
            align = "left",
            widget = wibox.widget.textbox,
        },
        1, 1, 1, 1
    )
    temps_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Current</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1, 2, 1, 1
    )
    temps_grid:add_widget_at(
        wibox.widget {
            markup = "<b>History (2 minutes)</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1, 3, 1, 1
    )

    local temperature_widget = wibox.widget {
        wibox.widget {
            text = " n/a",
            align = "center",
            widget = wibox.widget.textbox,
        },
        spacing = 4,
        layout = wibox.layout.fixed.horizontal,
        set_text = function(self, temps)
            local text = ""
            for _, val in ipairs(temps) do
                text = text .. string.format(" %s 糖 ", val.current)
            end
            self.children[1].text = " " .. text
        end,
    }

    for i, observer_config in ipairs(args) do
        watch(observer_config.command, observer_config.timeout or 1, function(w, stdout, _)
            local observed_data = watched_temps[i]
            if observed_data == nil then
                observed_data = {
                    name = observer_config.name,
                    current = 0,
                    name_widget = wibox.widget {
                        text = observer_config.name,
                        align = "left",
                        widget = wibox.widget.textbox,
                    },
                    current_widget = wibox.widget {
                        text = "n/a",
                        align = "center",
                        widget = wibox.widget.textbox,
                        set_temp = function(self, val)
                            self.text = string.format("%s 糖", val)
                        end
                    },
                    graph_widget = wibox.widget {
                        max_value = 100,
                        forced_width = common.calculate_graph_width_for_time(120, observer_config.graphstep_width or 2, observer_config.timeout or 1, observer_config.graphstep_spacing or 0),
                        background_color = observer_config.graphbg,
                        step_width = observer_config.graphstep_width or 2,
                        step_spacing = observer_config.graphstep_spacing or 0,
                        widget = wibox.widget.graph,
                        color = "linear:0,0:0,20:0," .. observer_config.graph_step3 .. ":0.3," .. observer_config.graph_step2 .. ":0.6," .. observer_config.graph_step1,
                    },
                }
                watched_temps[i] = observed_data
                temps_grid:add_widget_at(
                    watched_temps[i]["name_widget"],
                    i + 1, 1, 1, 1
                )
                temps_grid:add_widget_at(
                    watched_temps[i]["current_widget"],
                    i + 1, 2, 1, 1
                )
                temps_grid:add_widget_at(
                    {
                        watched_temps[i]["graph_widget"],
                        reflection = {horizontal = true},
                        layout = wibox.container.mirror
                    },
                    i + 1, 3, 1, 1
                )
            end
            observed_data.current = tonumber(stdout)
            observed_data.current_widget.temp = tostring(observed_data.current)
            observed_data.graph_widget:add_value(observed_data.current)

            w.text = watched_temps
        end, temperature_widget)
    end

    local popup = common.base_popup()

    temperature_widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if popup.visible then
                    popup.visible = not popup.visible
                else
                    popup:move_next_to(mouse.current_widget_geometry)
                end
            end)
        )
    )

    popup:setup {
        temps_grid,
        margins = 8,
        widget = wibox.container.margin
    }

    return temperature_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
