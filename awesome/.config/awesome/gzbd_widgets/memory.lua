
local wibox = require("wibox")
local watch = require("awful.widget.watch")
local gears = require("gears")
local awful = require("awful")

local common = require("gzbd_widgets.common")

local widget = {}

local total_mem_load_cmd = "sh -c \"free -m | grep '^Mem'\""
local mem_load_cmd = "sh -c \"ps -eo 'rss' -o '|%p|%C|' -o '%mem' -o '|%a' --sort=-%mem  | head -17 | tail -n +2\""

local function worker(args)
    local timeout = args.timeout or 1

    local mem_widget = wibox.widget {
        wibox.widget {
            text = " n/a",
            align = "center",
            widget = wibox.widget.textbox,
        },
        spacing = 4,
        layout = wibox.layout.fixed.horizontal,

        set_text = function(self, val)
            self.children[1].text = " " .. val
        end
    }

    watch(total_mem_load_cmd, timeout, function(w, stdout)
        for line in stdout:gmatch("[^\r\n]+") do
            local _, total, used, _, _, _, _ =
                line:match("(%w+:)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)")
            w.text = string.format("%d/%d MB", used, total)
        end
    end,
    mem_widget)

    local popup, popup_timer = common.popup_with_timer(timeout)

    mem_widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if popup.visible then
                    popup.visible = not popup.visible
                else
                    popup:move_next_to(mouse.current_widget_geometry)
                end
            end)
        )
    )

    local process_cmds = {}
    local process_grid = wibox.widget {
        layout = wibox.layout.grid.vertical,
        homogeneous = false,
        forced_num_cols = 5,
        vertical_spacing = 4,
        horizontal_spacing = 16,
    }
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>PID</b>",
            align = "right",
            widget = wibox.widget.textbox,
        },
        1,1,1,1
    )
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Name</b>",
            align = "left",
            widget = wibox.widget.textbox,
        },
        1,2,1,1
    )
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>CPU</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1,3,1,1
    )
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>MEM</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1,4,1,1
    )
    process_grid:add_widget_at(
        wibox.widget {
            markup = "<b>RSS</b>",
            align = "center",
            widget = wibox.widget.textbox,
        },
        1,5,1,1
    )
    for i = 2, 16 + 1, 1 do
        process_grid:add_widget_at(
            wibox.widget {
                text = "n/a",
                align = "right",
                widget = wibox.widget.textbox,
            },
            i,1,1,1
        )
        local comm_widget = wibox.widget {
            text = "n/a",
            align = "left",
            widget = wibox.widget.textbox,
        }
        process_grid:add_widget_at(
            comm_widget,
            i,2,1,1
        )
        process_grid:add_widget_at(
            wibox.widget {
                text = "n/a",
                align = "center",
                widget = wibox.widget.textbox,
            },
            i,3,1,1
        )
        process_grid:add_widget_at(
            wibox.widget {
                text = "n/a",
                align = "center",
                widget = wibox.widget.textbox,
            },
            i,4,1,1
        )
        process_grid:add_widget_at(
            wibox.widget {
                text = "n/a",
                align = "center",
                widget = wibox.widget.textbox,
            },
            i,5,1,1
        )

        awful.tooltip {
            objects = { process_grid:get_widgets_at(i, 2)[1] },
            mode = "mouse",
            timer_function = function()
                local text = process_cmds[i - 1]
                return text
                    :gsub('%s%-', '\n  -')
                    :gsub(':/', '\n    :/') -- java classpath uses : to separate jars
            end,
        }
    end

    popup_timer:connect_signal("timeout", function()
        awful.spawn.easy_async(mem_load_cmd, function(stdout, _, _, _)
            local i = 2 -- use order in process_grid
            for line in stdout:gmatch("[^\r\n]+") do
                local columns = common.split(line, '|')

                local rss = common.strip(columns[1]) -- in kb
                local rss = string.format("%.2f MB", tonumber(rss) / 1024)
                local pid = common.strip(columns[2])
                local cpu = common.strip(columns[3]) .. "%"
                local mem = common.strip(columns[4]) .. "%"
                local cmd = columns[5]
                local comm = common.basename(cmd)
                process_cmds[i - 1] = cmd

                process_grid:get_widgets_at(i,1)[1].text = pid
                process_grid:get_widgets_at(i,2)[1].text = comm
                process_grid:get_widgets_at(i,3)[1].text = cpu
                process_grid:get_widgets_at(i,4)[1].text = mem
                process_grid:get_widgets_at(i,5)[1].text = rss

                i = i + 1
            end
        end)
    end)

    popup:setup {
        process_grid,
        margins = 8,
        widget = wibox.container.margin
    }

    return mem_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
