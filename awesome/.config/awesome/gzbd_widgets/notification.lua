local wibox = require("wibox")
local gears = require("gears")
local awful = require("awful")
local naughty = require("naughty")
local os = require("os")

local widget = {}

local function worker(args)
    local start_time = nil
    -- Used to reset this setting to the value set by initially
    local expiration_paused = naughty.expiration_paused

    local notification_widget = wibox.widget {
        wibox.widget {
            text = " ",
            align = "center",
            widget = wibox.widget.textbox,
            id = "suspend",
        },
        spacing = 4,
        layout = wibox.layout.fixed.horizontal,
        set_text = function(self, value)
            local icon = start_time and " " or " "
            self.children[1].text = string.format("%s %s", icon, value)
        end
    }

    local notification_off_timer = gears.timer {
        timeout = 1,
        callback = function()
            local elapsed_seconds = os.time() - start_time
            local seconds = math.fmod(elapsed_seconds, 60)
            local minutes = math.floor(elapsed_seconds / 60)
            local hours = math.floor(elapsed_seconds / (60 * 60))
            notification_widget.text = string.format("%.2d:%.2d:%.2d (%d)", hours, minutes, seconds, #naughty.active)
        end
    }

    notification_widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if naughty.suspended then
                    notification_off_timer:stop()
                    start_time = nil
                    naughty.expiration_paused = expiration_paused
                    naughty.suspended = false
                    notification_widget.text = ""


                    -- Copy active notifications table
                    local active_notifications = {}
                    for i, n in ipairs(naughty.active) do
                        active_notifications[i] = n
                    end

                    -- Replay notifications that should be send during suspended
                    for _, n in ipairs(active_notifications) do
                        n.title = "(replayed) " .. n.title
                        naughty.notification({
                            title = n.title,
                            text = n.text,
                            timeout = n.timeout,
                            shape = n.shape,
                            opacity = n.opacity,
                            margin = n.margin,
                            preset = n.preset,
                            callback = n.callback,
                            actions = n.actions,
                        })
                    end
                else
                    start_time = os.time()
                    notification_off_timer:start()
                    notification_widget.text = "00:00:00 (0)"
                    -- We don't want to expire notifications send during suspend
                    naughty.expiration_paused = true
                    naughty.suspended = true
                end
            end)
        )
    )

    return notification_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
