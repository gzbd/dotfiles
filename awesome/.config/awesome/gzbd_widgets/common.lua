local awful = require("awful")
local gears = require("gears")

local common = {}

function common.split(string_to_split, separator)
    if separator == nil then separator = "%s" end
    local t = {}

    for str in string.gmatch(string_to_split, "([^".. separator .."]+)") do
        table.insert(t, str)
    end

    return t
end

function common.starts_with(str, start)
    return str:sub(1, #start) == start
end

function common.basename(str)
    local splitted_str = common.split(str, " ")
    if #splitted_str > 0 then
        str = splitted_str[1]
    end
    if common.starts_with(str, "[") then
        return str
    end
	return string.gsub(str, "(.*/)(.*)", "%2")
end

function common.strip(str)
    return str:gsub("%s+", "")
end

function common.calculate_graph_width_for_time(time_interval_seconds, step_width, timeout, step_spacing)
    return (step_width + step_spacing) * (time_interval_seconds / timeout)
end

function common.base_popup()
    local popup = awful.popup{
        ontop = true,
        visible = false,
        shape = gears.shape.rounded_rect,
        preferred_positions = "top",
        offset = { y = -8 },
        hide_on_right_click = true,
        widget = {}
    }

    return popup
end

function common.popup_with_timer(timeout)
    local popup = common.base_popup()
    local popup_timer = gears.timer({ timeout = timeout })

    popup:connect_signal("property::visible", function(w)
        if w.visible then
            popup_timer:start()
            popup_timer:emit_signal("timeout")
        else
            popup_timer:stop()
        end
    end)

    return popup, popup_timer

end

function common.list_directory(dir)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen("ls \"" .. dir .. "\"")
    for fname in pfile:lines() do
        i = i + 1
        t[i] = fname
    end
    pfile:close()
    return t
end

function common.first_line(path)
    local file, first = io.open(path, "rb"), nil
    if file then
        first = file:read("*l")
        file:close()
    end
    return first
end

return common
