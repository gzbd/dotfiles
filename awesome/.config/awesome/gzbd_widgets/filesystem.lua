local wibox = require("wibox")
local watch = require("awful.widget.watch")
local gears = require("gears")
local awful = require("awful")

local common = require("gzbd_widgets.common")

local widget = {}

local total_fs_cmd = "sh -c \"df -h --total --output=target,fstype,source,size,used,avail,pcent | tail -n +2 | LC_ALL=C sort\""

local function worker(args)
    local timeout = args.timeout or 15
    local alignment = args.alignment or "center"
    local value_alignment = args.value_alignment or "center"

    local filesystems = {}
    local filesystems_grid = wibox.widget {
        layout = wibox.layout.grid.vertical,
        forced_num_cols = 7,
        vertical_spacing = 4,
        horizontal_spacing = 16,
        homogeneous = false,
    }
    filesystems_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Mount path</b>",
            align = alignment,
            widget = wibox.widget.textbox,
        },
        1, 1, 1, 1
    )
    filesystems_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Type</b>",
            align = alignment,
            widget = wibox.widget.textbox,
        },
        1, 2, 1, 1
    )
    filesystems_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Filesystem</b>",
            align = alignment,
            widget = wibox.widget.textbox,
        },
        1, 3, 1, 1
    )
    filesystems_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Size</b>",
            align = value_alignment,
            widget = wibox.widget.textbox,
        },
        1, 4, 1, 1
    )
    filesystems_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Used</b>",
            align = value_alignment,
            widget = wibox.widget.textbox,
        },
        1, 5, 1, 1
    )
    filesystems_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Available</b>",
            align = value_alignment,
            widget = wibox.widget.textbox,
        },
        1, 6, 1, 1
    )
    filesystems_grid:add_widget_at(
        wibox.widget {
            markup = "<b>Used</b>",
            align = value_alignment,
            widget = wibox.widget.textbox,
        },
        1, 7, 1, 1
    )

    local filesystem_widget = wibox.widget {
        wibox.widget {
            text = " n/a",
            align = alignment,
            widget = wibox.widget.textbox,
        },
        spacing = 4,
        layout = wibox.layout.fixed.horizontal,
    }

    watch(total_fs_cmd, timeout, function(w, stdout)
        local i = 1
        for line in stdout:gmatch("[^\r\n]+") do
            if filesystems[i] == nil then
                filesystems[i] = {
                    mount_path_widget = wibox.widget {
                        text = "n/a",
                        align = alignment,
                        widget = wibox.widget.textbox,
                    },
                    type_widget = wibox.widget {
                        text = "n/a",
                        align = alignment,
                        widget = wibox.widget.textbox,
                    },
                    filesystem_widget = wibox.widget {
                        text = "n/a",
                        align = alignment,
                        widget = wibox.widget.textbox,
                    },
                    size_widget = wibox.widget {
                        text = "n/a",
                        align = value_alignment,
                        widget = wibox.widget.textbox,
                    },
                    used_widget = wibox.widget {
                        text = "n/a",
                        align = value_alignment,
                        widget = wibox.widget.textbox,
                    },
                    available_widget = wibox.widget {
                        text = "n/a",
                        align = value_alignment,
                        widget = wibox.widget.textbox,
                    },
                    usedp_widget = wibox.widget {
                        text = "n/a",
                        align = value_alignment,
                        widget = wibox.widget.textbox,
                    },
                }

                filesystems_grid:add_widget_at(
                    filesystems[i]["mount_path_widget"],
                    i + 1, 1, 1, 1
                )
                filesystems_grid:add_widget_at(
                    filesystems[i]["type_widget"],
                    i + 1, 2, 1, 1
                )
                filesystems_grid:add_widget_at(
                    filesystems[i]["filesystem_widget"],
                    i + 1, 3, 1, 1
                )
                filesystems_grid:add_widget_at(
                    filesystems[i]["size_widget"],
                    i + 1, 4, 1, 1
                )
                filesystems_grid:add_widget_at(
                    filesystems[i]["used_widget"],
                    i + 1, 5, 1, 1
                )
                filesystems_grid:add_widget_at(
                    filesystems[i]["available_widget"],
                    i + 1, 6, 1, 1
                )
                filesystems_grid:add_widget_at(
                    filesystems[i]["usedp_widget"],
                    i + 1, 7, 1, 1
                )
            end

            local path, type, name, size, used, avail, usedp = line:match("([/%w%-]+)%s+([%w%d%-]+)%s+([/%w%-]+)%s+(%d[%.%d%w]*)%s+(%d[%.%d%w]*)%s+(%d[%.%d%w]*)%s+(%d[%.%d%%]*)")

            filesystems[i]["usedp"] = common.strip(usedp)
            filesystems[i]["mount_path_widget"].text = common.strip(path)
            filesystems[i]["type_widget"].text = common.strip(type)
            filesystems[i]["filesystem_widget"].text = common.strip(name)
            filesystems[i]["size_widget"].text = common.strip(size)
            filesystems[i]["used_widget"].text = common.strip(used)
            filesystems[i]["available_widget"].text = common.strip(avail)
            filesystems[i]["usedp_widget"].text = common.strip(usedp)

            i = i + 1
        end

        w.text = " " .. common.strip(filesystems[1].usedp)
    end, filesystem_widget.children[1])

    local popup = common.base_popup()

    filesystem_widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if popup.visible then
                    popup.visible = not popup.visible
                else
                    popup:move_next_to(mouse.current_widget_geometry)
                end
            end)
        )
    )

    popup:setup {
        filesystems_grid,
        margins = 8,
        widget = wibox.container.margin
    }

    return filesystem_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
