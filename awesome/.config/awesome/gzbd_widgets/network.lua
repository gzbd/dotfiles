local wibox = require("wibox")
local gears = require("gears")

local common = require("gzbd_widgets.common")

local widget = {}


local function worker(args)
    local main_interface = args.main_interface
    local timeout = args.timeout or 1
    local unit = args.unit or 1024 -- KB

    local interfaces = {}
    local network_widget = wibox.widget {
        wibox.widget {
            text = " n/a",
            align = "center",
            widget = wibox.widget.textbox,
        },
        spacing = 4,
        layout = wibox.layout.fixed.horizontal,
        set_text = function(self, val)
            self.children[1].text = " " .. val
        end
    }

    gears.timer {
        timeout = timeout,
        autostart = true,
        callback = function()
            local ifnames = common.list_directory("/sys/class/net")
            for _, ifname in ipairs(ifnames) do
                local ifdata = interfaces[ifname] or {}

                ifdata.prev_rxbytes = ifdata.rxbytes or 0
                ifdata.prev_txbytes = ifdata.txbytes or 0
                ifdata.rxbytes = tonumber(common.first_line(string.format("/sys/class/net/%s/statistics/rx_bytes", ifname)))
                ifdata.txbytes = tonumber(common.first_line(string.format("/sys/class/net/%s/statistics/tx_bytes", ifname)))
                ifdata.now_rx = (ifdata.rxbytes - ifdata.prev_rxbytes) / unit
                ifdata.now_tx = (ifdata.txbytes - ifdata.prev_txbytes) / unit

                interfaces[ifname] = ifdata
            end

            local main_ifdata = interfaces[main_interface]
            network_widget.text = string.format("%.1f KB/s  %.1f KB/s  ", main_ifdata.now_rx, main_ifdata.now_tx)
        end
    }

    return network_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
