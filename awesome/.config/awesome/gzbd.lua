local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local gfs = require("gears.filesystem")
local naughty = require("naughty")

local volume_widget = require("gzbd_widgets.volume")
local backlight_widget = require("gzbd_widgets.backlight")
local cpuusage_widget = require("gzbd_widgets.cpu")
local memusage_widget = require("gzbd_widgets.memory")
local fsusage_widget = require("gzbd_widgets.filesystem")
local notification_widget = require("gzbd_widgets.notification")
local network_widget = require("gzbd_widgets.network")
local temps_widget = require("gzbd_widgets.temperature")

local lain = require("lain")

math.randomseed(os.time());

awesome.set_preferred_icon_size(128)

local themes_path                  = gfs.get_themes_dir()
local theme                        = {}

theme.lain_icons                   = os.getenv("HOME") .. "/.config/awesome/lain/icons/layout/default/"

local dpi                          = xresources.apply_dpi

local markup                       = lain.util.markup

theme.font                         = "JetBrainsMono NF SemiBold 10"
theme.icon_size                    = 24

theme.colors                       = {}
theme.colors.background            = "#161821"
theme.colors.background_light      = "#1e2132"
theme.colors.background_light_text = "#444b71"
theme.colors.foreground            = "#c6c8d1"
theme.colors.foreground_dark       = "#3e445e"
theme.colors.red                   = "#e27878"
theme.colors.yellow                = "#e2a478"
theme.colors.green                 = "#b4be82"
theme.colors.teal                  = "#89b8c2"
theme.colors.blue                  = "#84a0c6"
theme.colors.pink                  = "#a093c7"
theme.colors.gray                  = "#6b7089"

theme.bg_normal                    = theme.colors.background_light
theme.bg_focus                     = theme.colors.foreground_dark
theme.bg_urgent                    = theme.colors.yellow

theme.fg_normal                    = theme.colors.gray
theme.fg_focus                     = theme.colors.foreground
theme.fg_urgent                    = theme.colors.background_light
theme.fg_minimize                  = theme.colors.foreground

theme.useless_gap                  = dpi(16)
theme.border_width                 = dpi(4)
theme.border_normal                = theme.colors.background
theme.border_focus                 = theme.colors.blue
theme.border_marked                = theme.colors.teal

theme.prompt_bg                    = theme.bg_focus
theme.prompt_fg                    = theme.fg_focus
theme.prompt_bg_cursor             = theme.bg_focus
theme.prompt_fg_cursor             = theme.bg_focus

theme.systray_icon_spacing         = dpi(6)
theme.systray_icon_size            = dpi(16)

theme.notification_bg              = theme.colors.background_light
theme.notification_fg              = theme.colors.foreground
theme.notification_width           = dpi(384)
theme.notification_max_height      = dpi(512)
theme.notification_icon_size       = dpi(64)
theme.notification_shape           = gears.shape.rounded_rect

theme.mstab_bar_disable            = true

naughty.config.padding             = dpi(8)
naughty.config.spacing             = dpi(8)
naughty.config.icon_dirs           = {
    "/usr/share/pixmaps/"
}
naughty.config.notify_callback     = function(args)
    if args.app_name == "Slack" then
        args.icon = "/usr/share/pixmaps/slack.png"
    end
    if args.app_name == "mutt-wizard" then
        args.icon = "/usr/share/doc/neomutt/logo/neomutt.svg"
    end
    return args
end


-- Returns all files (except . and ..) in "directory"
local function scandir(directory)
    local num_files, t, popen = 0, {}, io.popen
    for filename in popen('ls -a "' .. directory .. '"'):lines() do
        if (not (filename == "." or filename == "..")) then
            num_files = num_files + 1
            t[num_files] = filename
        end
    end
    return t
end

local wallpapers_dir = awful.util.getdir("config") .. "wallpapers/3440-series"
theme.wallpaper = function()
    local choices = scandir(wallpapers_dir)
    local choices_size = 0
    for _ in pairs(choices) do choices_size = choices_size + 1 end
    local position = math.random(1, choices_size)
    return wallpapers_dir .. "/" .. choices[position]
end

local function rrect(radius)
    return function(cr, width, height)
        gears.shape.rounded_rect(cr, width, height, radius)
    end
end

theme.wibar_height                              = dpi(32)

theme.border_radius                             = dpi(6)

theme.taglist_bg_focus                          = theme.bg_focus
theme.taglist_fg_focus                          = theme.fg_focus
theme.taglist_fg_empty                          = theme.fg_normal
theme.taglist_fg_occupied                       = theme.colors.blue
theme.taglist_fg_urgent                         = theme.colors.red
theme.taglist_disable_icon                      = true
theme.taglist_shape_focus                       = rrect(theme.border_radius)

theme.tasklist_bg_focus                         = theme.bg_focus
theme.tasklist_fg_focus                         = theme.fg_focus
theme.tasklist_bg_normal                        = theme.colors.background
theme.tasklist_fg_normal                        = theme.fg_normal
theme.tasklist_fg_urgent                        = theme.colors.red
theme.tasklist_disable_icon                     = true
theme.tasklist_spacing                          = dpi(6)

-- Define the image to load
theme.titlebar_close_button_normal              = themes_path .. "default/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = themes_path .. "default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal           = themes_path .. "default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = themes_path .. "default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive     = themes_path .. "default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = themes_path .. "default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = themes_path .. "default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = themes_path .. "default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive    = themes_path .. "default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = themes_path .. "default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = themes_path .. "default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = themes_path .. "default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive  = themes_path .. "default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = themes_path .. "default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = themes_path .. "default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = themes_path .. "default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path .. "default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path .. "default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = themes_path .. "default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = themes_path .. "default/titlebar/maximized_focus_active.png"

-- You can use your own layout icons like this:
theme.layout_fairh                              = themes_path .. "default/layouts/fairhw.png"
theme.layout_fairv                              = themes_path .. "default/layouts/fairvw.png"
theme.layout_floating                           = themes_path .. "default/layouts/floatingw.png"
theme.layout_magnifier                          = themes_path .. "default/layouts/magnifierw.png"
theme.layout_max                                = themes_path .. "default/layouts/maxw.png"
theme.layout_fullscreen                         = themes_path .. "default/layouts/fullscreenw.png"
theme.layout_tilebottom                         = themes_path .. "default/layouts/tilebottomw.png"
theme.layout_tileleft                           = themes_path .. "default/layouts/tileleftw.png"
theme.layout_tile                               = themes_path .. "default/layouts/tilew.png"
theme.layout_tiletop                            = themes_path .. "default/layouts/tiletopw.png"
theme.layout_spiral                             = themes_path .. "default/layouts/spiralw.png"
theme.layout_dwindle                            = themes_path .. "default/layouts/dwindlew.png"
theme.layout_cornernw                           = themes_path .. "default/layouts/cornernww.png"
theme.layout_cornerne                           = themes_path .. "default/layouts/cornernew.png"
theme.layout_cornersw                           = themes_path .. "default/layouts/cornersww.png"
theme.layout_cornerse                           = themes_path .. "default/layouts/cornersew.png"
theme.layout_termfair                           = theme.lain_icons .. "termfairw.png"
theme.layout_centerfair                         = theme.lain_icons .. "centerfairw.png"  -- termfair.center
theme.layout_cascade                            = theme.lain_icons .. "cascadew.png"
theme.layout_cascadetile                        = theme.lain_icons .. "cascadetilew.png" -- cascade.tile
theme.layout_centerwork                         = theme.lain_icons .. "centerworkw.png"
theme.layout_centerworkh                        = theme.lain_icons .. "centerworkhw.png" -- centerwork.horizontal


theme.gzbd_layouts_icons             = os.getenv("HOME") .. "/.config/awesome/gzbd_layouts/icons/"
theme.layout_centered_stacked        = theme.gzbd_layouts_icons .. "centered_stacked.png"
theme.layout_ultrawide_mstab         = theme.gzbd_layouts_icons .. "ultrawide_mstab.png"

theme.centered_stacked_client_width  = 2560
theme.centered_stacked_client_height = 1440
theme.ultrawide_mstab_area_width     = 2560

-- bling settings
theme.flash_focus_start_opacity      = 0.75
theme.flash_focus_step               = 0.04
theme.parent_filter_list             = { "firefox", "Gimp" }
theme.swallowing_filter              = true

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.centered(wallpaper, s)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- And after 30 min change it
gears.timer {
    timeout = 60 * 30, -- seconds
    autostart = true,
    callback = function() set_wallpaper(nil) end
}


-- Widgets
local systray = wibox.widget.systray()
systray:set_base_size(theme.icon_size)

os.setlocale(os.getenv("LANG")) -- to localize the clock
local clock_widget = wibox.widget.textclock(markup.fontfg(theme.font, theme.fg_normal, "%A %d %B %H:%M"), 1)

local mail_widget, _ = awful.widget.watch(
    "sh -c \"ls -A ~/.local/share/mail/personal/INBOX/new | wc -l\"",
    1,
    function(widget, stdout)
        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, " " .. stdout))
    end
)
local archupdates_widget, _ = awful.widget.watch(
    "zsh -c \"{checkupdates 2> /dev/null & yay -Qum 2> /dev/null} | wc -l\"",
    600,
    function(widget, stdout)
        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, " " .. stdout))
    end
)

local battery_widget = lain.widget.bat({
        timeout = 5,
        n_perc = { 10, 20 },
        settings = function()
            local perc = bat_now.perc ~= "N/A" and bat_now.perc .. "%" or bat_now.perc
            local icon = "?"
            if bat_now.perc ~= "N/A" then
                if bat_now.perc > 80 then
                    icon = ""
                elseif bat_now.perc > 60 then
                    icon = ""
                elseif bat_now.perc > 40 then
                    icon = ""
                elseif bat_now.perc > 20 then
                    icon = ""
                else
                    icon = ""
                end
            end

            widget:set_markup(markup.fontfg(theme.font, theme.fg_normal,
                perc .. " " .. icon .. "  " .. bat_now.watt .. " W " .. bat_now.time))
        end
    }).widget

local cpufan_widget, _ = awful.widget.watch(
    "/home/gzbd/.config/polybar/scripts/cpu-fan.sh",
    10,
    function(widget, stdout)
        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, stdout))
    end
)

local pulse_volume_widget = volume_widget({
    font = theme.font,
    fg = theme.fg_normal,
    bg = theme.bg_normal,
    gray = theme.fg_normal,
    bar_color = theme.colors.blue
})

local screen_backlight_w = backlight_widget({
    font = theme.font,
    fg = theme.fg_normal,
    bg = theme.bg_normal,
    bar_color = theme.colors.blue,
    device = "sysfs/backlight/amdgpu_bl1"
})

local keyboard_backlight_w = backlight_widget({
    font = theme.font,
    fg = theme.fg_normal,
    bg = theme.bg_normal,
    bar_color = theme.colors.blue,
    device = "sysfs/leds/tpacpi::kbd_backlight",
    icon = "  "
})

function theme.at_screen_connect(s)
    set_wallpaper(s)

    local tags = awful.tag({ "web", "mail", "chat", "music", "files", "term", "vim" }, s, awful.layout.layouts[7])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    s.mypromptbox.shape = rrect(theme.border_radius)
    s.mypromptbox.widget = {
        s.mypromptbox.widget,
        left = dpi(6),
        right = dpi(6),
        visible = false,
        widget = wibox.container.margin,
    }
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
        awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 4, function() awful.layout.inc(1) end),
        awful.button({}, 5, function() awful.layout.inc(-1) end)))

    s.mastercount = wibox.widget {
        id = 'master_client_count',
        widget = wibox.widget.textbox,
        markup = tostring(markup.font("JetBrainsMono NF SemiBold 8", tostring(tags[1].master_count)))
    }

    for _, t in ipairs(tags) do
        t:connect_signal("property::selected", function(t)
            if t.selected then
                t.screen.mastercount:set_markup(markup.font("JetBrainsMono NF SemiBold 8",
                    tostring(t.master_count)))
            end
        end)
        t:connect_signal("property::master_count", function(t, v)
            if t.selected then
                t.screen.mastercount:set_markup(markup.font("JetBrainsMono NF SemiBold 8",
                    tostring(t.master_count)))
            end
        end)
    end
    -- Create a taglist widget
    s.mytaglist = {
        awful.widget.taglist {
            screen          = s,
            filter          = awful.widget.taglist.filter.all,
            buttons         = awful.util.taglist_buttons,
            style           = {
                shape = rrect(theme.border_radius),
            },
            layout          = { spacing = dpi(5), layout = wibox.layout.fixed.horizontal },
            widget_template = {
                {
                    {
                        nil,
                        {
                            { id = 'text_role', widget = wibox.widget.textbox },
                            left = dpi(3),
                            right = dpi(3),
                            widget = wibox.container.margin
                        },
                        {
                            {
                                id = 'index_role',
                                widget = wibox.widget.textbox,
                            },
                            top = dpi(6),
                            widget = wibox.container.margin
                        },
                        layout = wibox.layout.fixed.horizontal
                    },
                    left = dpi(3),
                    right = dpi(3),
                    widget = wibox.container.margin
                },
                id = 'background_role',
                widget = wibox.container.background,
                create_callback = function(self, t, index, objects) --luacheck: no unused args
                    self:get_children_by_id('index_role')[1].markup = markup.font("JetBrainsMono NF SemiBold 8",
                        index)
                end,
                update_callback = function(self, t, index, objects) --luacheck: no unused args
                    self:get_children_by_id('index_role')[1].markup = markup.font("JetBrainsMono NF SemiBold 8",
                        index)
                end,
            }
        },
        bg = theme.colors.background,
        shape = rrect(theme.border_radius),
        widget = wibox.container.background
    }

    -- Create a tasklist widget

    s.mytasklist = awful.widget.tasklist {
        screen          = s,
        filter          = awful.widget.tasklist.filter.currenttags,
        buttons         = awful.util.tasklist_buttons,
        style           = {
            shape = rrect(theme.border_radius),
        },
        layout          = {
            spacing = dpi(6),
            layout = wibox.layout.flex.horizontal,
        },
        widget_template = {
            {
                {
                    {
                        {
                            id = "text_role",
                            widget = wibox.widget.textbox,
                        },
                        left = dpi(3),
                        right = dpi(3),
                        widget = wibox.container.margin
                    },
                    layout = wibox.layout.flex.horizontal,
                },
                left = dpi(3),
                right = dpi(3),
                widget = wibox.container.margin
            },
            id     = "background_role",
            widget = wibox.container.background,
        },
    }

    -- Create the wibox
    s.topwibox = awful.wibar({ position = "top", screen = s })
    s.bottomwibox = awful.wibar({ position = "bottom", screen = s })

    -- Add widgets to the wibox
    s.topwibox:setup {
        layout = wibox.layout.align.horizontal,
        {
          -- Left widgets
            {
                {
                    {
                        s.mylayoutbox,
                        left = dpi(3),
                        right = dpi(3),
                        widget = wibox.container.margin
                    },
                    {
                        s.mastercount,
                        top = dpi(8),
                        widget = wibox.container.margin,
                    },
                    nil,
                    layout = wibox.layout.fixed.horizontal,
                },
                s.mytaglist,
                s.mypromptbox,
                spacing = dpi(3),
                layout = wibox.layout.fixed.horizontal,
            },
            margins = dpi(6),
            widget = wibox.container.margin,
        },
        { s.mytasklist, margins = dpi(6), widget = wibox.container.margin },
        {
          -- Right widgets
            {
                {
                    systray,
                    right = dpi(12),
                    widget = wibox.container.margin
                },
                {
                    {
                        clock_widget,
                        left = dpi(4),
                        right = dpi(4),
                        widget = wibox.container.margin
                    },
                    bg = theme.colors.background,
                    shape = rrect(theme.border_radius),
                    widget = wibox.container.background
                },
                layout = wibox.layout.align.horizontal,
                spacing = dpi(4),
            },
            margins = dpi(6),
            widget = wibox.container.margin,
        }
    }

    s.bottomwibox:setup {
        {
            {
                {
                    mail_widget,
                    archupdates_widget,
                    cpuusage_widget({
                        graphbg = theme.colors.background,
                        graph_step1 = theme.colors.green,
                        graph_step2 = theme.colors.yellow,
                        graph_step3 = theme.colors.red,
                    }),
                    temps_widget({
                        {
                            command =
                            "sh -c \"sensors zenpower-pci-00c3 | grep 'Tctl' | awk -F: '{ print $2 }' | grep -oE '[[:digit:]]+' | head -1\"",
                            name = "CPU",
                            graphbg = theme.colors.background,
                            graph_step1 = theme.colors.green,
                            graph_step2 = theme.colors.yellow,
                            graph_step3 = theme.colors.red,
                        },
                        -- {
                        --     command = "sh -c \"nvidia-smi -q -d temperature | grep 'GPU Current Temp' | awk -F: '{ print $2 }' | grep -oE '[[:digit:]]+'\"",
                        --     name = "GPU",
                        --     graphbg = theme.colors.background,
                        --     graph_step1 = theme.colors.green,
                        --     graph_step2 = theme.colors.yellow,
                        --     graph_step3 = theme.colors.red,
                        -- }
                    }),
                    cpufan_widget,
                    memusage_widget({}),
                    network_widget({ main_interface = "enp6s0f3u1u4" }),
                    battery_widget,
                    fsusage_widget({ alignment = "left" }),
                    pulse_volume_widget,
                    screen_backlight_w,
                    keyboard_backlight_w,
                    notification_widget,
                    spacing = dpi(32),
                    layout = wibox.layout.fixed.horizontal,
                },
                left = dpi(6),
                right = dpi(6),
                top = dpi(3),
                bottom = dpi(3),
                widget = wibox.container.margin
            },
            bg = theme.colors.background,
            shape = rrect(theme.border_radius),
            widget = wibox.container.background,
        },
        layout = wibox.container.place,
    }
end

return theme
