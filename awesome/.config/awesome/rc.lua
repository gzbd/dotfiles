-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox                     = require("wibox")
-- Theme handling library
local beautiful                 = require("beautiful")
-- Notification library
local naughty                   = require("naughty")
local menubar                   = require("menubar")
local hotkeys_popup             = require("awful.hotkeys_popup")
local lain                      = require("lain")
local centered_stacked          = require("gzbd_layouts.centered_stacked")
local ultrawide_mstab           = require("gzbd_layouts.ultrawide_mstab")
local markup                    = lain.util.markup

naughty.config.defaults.timeout = 10

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err)
        })
        in_error = false
    end)
end
-- }}}
-- This function will run once every time Awesome is started
local function run_once(cmd_arr)
    for _, cmd in ipairs(cmd_arr) do
        awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
    end
end

run_once({
    "sleep 1 && autorandr -c docked",
    "unclutter -root",
    "light-locker",
    "redshift-qt",
    "caffeine",
    -- "picom --experimental-backends",
    "picom",
    "blueman-applet",
}) -- entries must be separated by commas

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(string.format("%s/.config/awesome/gzbd.lua", os.getenv("HOME")))

local bling = require("bling")
-- bling.module.flash_focus.enable()
-- bling.module.window_swallowing.start()
-- This is used later as the default terminal and editor to run.
terminal             = "alacritty"
editor               = "nvim"
editor_cmd           = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and mod1.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey               = "Mod4"
altkey               = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    bling.layout.mstab,
    centered_stacked,
    ultrawide_mstab
}
-- }}}

client.connect_signal("focus",
    function(c)
        if c.maximized_horizontal == true and c.maximized_vertical == true then
            c.border_color = beautiful.border_normal
        else
            c.border_color = beautiful.border_focus
        end
    end
)

client.connect_signal("unfocus",
    function(c)
        c.border_color = beautiful.border_normal
    end
)

local function arrange(s)
    local clients = awful.client.visible(s)
    local layout  = awful.layout.getname(awful.layout.get(s))

    if #clients > 0 then              -- Fine grained borders and floaters control
        for _, c in pairs(clients) do -- Floaters always have borders
            if c.floating or layout == "floating" then
                c.border_width = beautiful.border_width
            elseif (layout == "centered_stacked") then
                c.border_width = 0
                screen[s].selected_tag.gap = beautiful.useless_gap
                -- No borders and gaps with only one visible client
            elseif (#clients == 1 or layout == "max" or layout == "fullscreen") and
                (layout ~= "centerwork" and layout ~= "centerfair" and layout ~= "ultrawide_mstab") then
                c.border_width = 0
                screen[s].selected_tag.gap = 0
            else
                c.border_width = beautiful.border_width
                screen[s].selected_tag.gap = beautiful.useless_gap
            end
        end
    end
end

-- Arrange signal handler
for s = 1, screen.count() do screen[s]:connect_signal("arrange", arrange) end
screen.connect_signal("added", function(s)
    s:connect_signal("arrange", arrange)
end
)

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
    { "hotkeys",     function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
    { "manual",      terminal .. " -e man awesome" },
    { "edit config", editor_cmd .. " " .. awesome.conffile },
    { "restart",     awesome.restart },
    { "quit",        function() awesome.quit() end },
}

mymainmenu = awful.menu({
    items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
        { "open terminal", terminal }
    }
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

awful.util.taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t) t:view_only() end),
    awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end)
)

awful.util.tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                { raise = true }
            )
        end
    end),

    awful.button({}, 4, function() awful.client.focus.byidx(1) end),
    awful.button({}, 5, function() awful.client.focus.byidx(-1) end)
)

awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s) end)

-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({}, 3, function() mymainmenu:toggle() end)
))
-- }}}

-- Focus window regardless if it is not visible at the moment
function focus_by_idx(i, sel)
    sel = sel or client.focus
    if sel then
        local cls = client.get(sel.screen)
        local tags = sel.screen.selected_tags
        -- Compile a list of all candidate clients
        local candidates = {}
        for _, c in pairs(cls) do
            if awful.client.focus.filter(c) or sel == c then
                local found = false
                for _, t in pairs(c:tags()) do
                    if gears.table.hasitem(tags, t) then
                        found = true
                    end
                end
                if found then
                    table.insert(candidates, c)
                end
            end
        end
        -- Now find the next client
        local idx = gears.table.hasitem(candidates, sel)
        local target = candidates[gears.math.cycle(#candidates, idx + i)]
        -- and focus it
        if target then
            target.minimized = false
            target:emit_signal("request::activate", "client.focus.byidx",
                { raise = true })
        end
    end
end

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey, }, "s", hotkeys_popup.show_help,
        { description = "show help", group = "awesome" }),
    -- Client manipulation
    awful.key({ modkey }, "h",
        function()
            awful.client.focus.global_bydirection("left", nil, true)
        end,
        { description = "focus next left client", group = "client" }
    ),

    awful.key({ modkey }, "j",
        function()
            awful.client.focus.global_bydirection("down", nil, true)
        end,
        { description = "focus next below client", group = "client" }
    ),

    awful.key({ modkey }, "k",
        function()
            awful.client.focus.global_bydirection("up", nil, true)
        end,
        { description = "focus next upper client", group = "client" }
    ),

    awful.key({ modkey }, "l",
        function()
            awful.client.focus.global_bydirection("right", nil, true)
        end,
        { description = "focus next right client", group = "client" }
    ),

    awful.key({ modkey }, "n",
        function(c)
            focus_by_idx(1, c)
        end,
        { description = "focus next client", group = "client" }
    ),

    awful.key({ modkey }, "p",
        function(c)
            focus_by_idx(-1, c)
        end,
        { description = "focus previous client", group = "client" }
    ),

    awful.key({ modkey, altkey }, "h", function() awful.client.swap.bydirection("left", client.focus) end,
        { description = "swap with next client on left", group = "client" }),
    awful.key({ modkey, altkey }, "j", function() awful.client.swap.bydirection("down", client.focus) end,
        { description = "swap with next client on bottom", group = "client" }),
    awful.key({ modkey, altkey }, "k", function() awful.client.swap.bydirection("up", client.focus) end,
        { description = "swap with next client on top", group = "client" }),
    awful.key({ modkey, altkey }, "l", function() awful.client.swap.bydirection("right", client.focus) end,
        { description = "swap with next client on right", group = "client" }),
    awful.key({ modkey, altkey }, "n", function() awful.client.swap.byidx(1) end,
        { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, altkey }, "p", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous client by index", group = "client" }),

    -- Tag manipulation
    awful.key({ modkey, "Control" }, "h",
        function()
            awful.tag.viewprev()
        end,
        { description = "focus next left screen", group = "tag" }
    ),

    awful.key({ modkey, "Control" }, "l",
        function()
            awful.tag.viewnext()
        end,
        { description = "focus next right screen", group = "tag" }
    ),

    awful.key({ modkey, "Control" }, "d",
        function()
            local t = awful.screen.focused().selected_tag
            if not t then return end
            t:delete()
        end,
        { description = "delete focused tag", group = "tag" }
    ),

    awful.key({ modkey, "Control" }, "n",
        function()
            local promptbox_widget = awful.screen.focused().mypromptbox.widget
            promptbox_widget.visible = true

            awful.prompt.run({
                prompt        = "New tag name: ",
                textbox       = promptbox_widget.widget,
                exe_callback  = function(name)
                    if not name or #name == 0 then return end

                    local tag = awful.tag.add(name, {
                        screen = awful.screen.focused(),
                        layout = awful.layout.suit.tile.left,
                    })
                    tag:view_only()
                    tag:connect_signal("property::selected", function(t)
                        if t.selected then
                            t.screen.mastercount:set_markup(markup.font("JetBrainsMono Nerd Font SemiBold 8",
                                tostring(t.master_count)))
                        end
                    end)
                    tag:connect_signal("property::master_count", function(t, v)
                        if t.selected then
                            t.screen.mastercount:set_markup(markup.font("JetBrainsMono Nerd Font SemiBold 8",
                                tostring(t.master_count)))
                        end
                    end)
                end,
                done_callback = function()
                    promptbox_widget.visible = false
                end
            })
        end,
        { description = "create new tag with prompt", group = "tag" }),

    -- Layout manipulation
    awful.key({ modkey }, "r", function()
        awful.screen.focused().selected_tag.master_width_factor = 0.5
    end, { description = "restore layout master width factor", group = "layout" }),
    awful.key({ modkey }, "space", function() awful.layout.inc(1) end,
        { description = "select next", group = "layout" }),
    awful.key({ modkey, "Shift" }, "space", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" }),
    awful.key({ modkey }, "i", function() awful.tag.incnmaster(1, nil, false) end,
        { description = "increase the number of master clients", group = "layout" }),
    awful.key({ modkey, "Shift" }, "i", function() awful.tag.incnmaster(-1, nil, false) end,
        { description = "decrease the number of master clients", group = "layout" }),

    -- Standard program
    awful.key({}, "Print", function() awful.spawn("flameshot gui") end,
        { description = "Capture screenshot", group = "launcher" }),
    awful.key({ altkey }, "m", function() awful.spawn("amixer set Master toggle") end,
        { description = "Toggle sound", group = "launcher" }),

    -- Launchers
    awful.key({ modkey }, "Return", function() awful.spawn(terminal) end,
        { description = "open a terminal", group = "launcher" }),
    awful.key({ modkey }, "v", function() awful.spawn("gtk-launch neovide") end,
        { description = "open a neovide text editor", group = "launcher" }),

    awful.key({ modkey }, "x",
        function() awful.spawn("rofi -show combi -combi-modi \"window,drun\" -modi combi -show-icons") end,
        { description = "rofi program launcher", group = "launcher" }),

    awful.key({ modkey }, "e",
        function()
            awful.spawn(
                "/usr/bin/rofi -modi \"dictionary:~/.config/rofi/scripts/dictionary.sh EN\" -show dictionary")
        end
        ,
        { description = "english-polish dictionary launcher", group = "launcher" }),

    -- awful.key({ modkey }, "p", function() awful.spawn("/usr/bin/rofi -modi \"dictionary:~/.config/rofi/scripts/dictionary.sh PL\" -show dictionary") end,
    --           { description = "polish-english dictionary launcher", group = "launcher" }),

    awful.key({ modkey, "Shift" }, "l", function() awful.spawn("light-locker-command -l") end,
        { description = "Screen locker", group = "launcher" }),

    -- Multimedia keys
    -- sink
    awful.key({}, "XF86AudioRaiseVolume", function() awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ +1%") end,
        { description = "Default sink volume up", group = "Multimedia" }),
    awful.key({}, "XF86AudioLowerVolume", function() awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ -1%") end,
        { description = "Default sink volume down", group = "Multimedia" }),
    awful.key({}, "XF86AudioMute", function() awful.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle") end,
        { description = "Default sink mute toggle", group = "Multimedia" }),
    -- source
    awful.key({ "Control" }, "XF86AudioRaiseVolume",
        function() awful.spawn("pactl set-source-volume @DEFAULT_SOURCE@ +1%") end,
        { description = "Default source volume up", group = "Multimedia" }),
    awful.key({ "Control" }, "XF86AudioLowerVolume",
        function() awful.spawn("pactl set-source-volume @DEFAULT_SOURCE@ -1%") end,
        { description = "Default source volume down", group = "Multimedia" }),
    awful.key({ "Control" }, "XF86AudioMute", function() awful.spawn("pactl set-source-mute @DEFAULT_SOURCE@ toggle") end,
        { description = "Default source mute toggle", group = "Multimedia" })
)

clientkeys = gears.table.join(
    awful.key({ modkey, }, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle fullscreen", group = "client" }),
    awful.key({ modkey, "Shift" }, "c", function(c) c:kill() end,
        { description = "close", group = "client" }),
    awful.key({ modkey, "Control" }, "space", awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
        { description = "swap with master", group = "client" }),
    awful.key({ modkey, "Control" }, "m", function(c) awful.client.setmaster(c) end,
        { description = "move to master", group = "client" }),
    awful.key({ modkey, "Control" }, "s", function(c) awful.client.setslave(c) end,
        { description = "move to slave", group = "client" }),
    awful.key({ modkey, }, "o", function(c) c:move_to_screen() end,
        { description = "move to screen", group = "client" }),
    awful.key({ modkey, "Shift" }, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        { description = "(un)maximize", group = "client" }),
    awful.key({ modkey }, "m", function(c) c.minimized = not c.minimized end,
        { description = "minimize client", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            { description = "view tag #" .. i, group = "tag" }),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            { description = "move focused client to tag #" .. i, group = "tag" })
    )
end

clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA",   -- Firefox addon DownThemAll.
                "copyq", -- Includes session name in class.
                "pinentry",
            },
            class = {
                "Arandr",
                "Blueman-manager",
                "Gpick",
                "Kruler",
                "MessageWin",  -- kalarm.
                "Sxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "Wpa_gui",
                "veromix",
                "xtightvncviewer"
            },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
                "Event Tester", -- xev.
            },
            role = {
                "AlarmWindow",   -- Thunderbird's calendar.
                "ConfigManager", -- Thunderbird's about:config.
                "pop-up",        -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = { floating = true }
    },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = { type = { "normal", "dialog" }
        },
        properties = { titlebars_enabled = false }
    },
    {
        rule_any = { class = { "Dictionary" } },
        callback = function(c)
            c.floating = true
            c.width = 800
            c.height = 600
            c.x = math.floor((c.screen.workarea.width - 800) / 2)
            c.y = math.floor((c.screen.workarea.height - 600) / 2)
        end
    }

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({}, 1, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.move(c)
        end),
        awful.button({}, 3, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c):setup {
        {
            -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        {
            -- Middle
            {
                -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        {
            -- Right
            awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton(c),
            awful.titlebar.widget.ontopbutton(c),
            awful.titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
