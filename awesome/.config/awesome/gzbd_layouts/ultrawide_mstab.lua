-- Based on bling.layout.mstab but with removed tab bar and adds wide horizontal
-- padding. Useful when using ultra-wide monitor. You have clients in the center of a
-- screen, unlike left or right side of a monitor and moving your eyeballs.

--- The centered_stacked layout layoutbox icon.
-- @beautiful beautiful.layout_ultrawide_mstab
-- @param surface
-- @see gears.surface

local beautiful = require("beautiful")

local ultrawide_mstab = {}

ultrawide_mstab.name = "ultrawide_mstab"

-- The top_idx is the idx of the slave clients (excluding all master clients)
-- that should be on top of all other slave clients ("the focused slave")
-- by creating a variable outside of the arrange function, this layout can "remember" that client
-- by creating it as a new property of every tag, this layout can be active on different tags and
-- still have different "focused slave clients"
for idx, tag in ipairs(root.tags()) do
    tag.top_idx = 1
end

-- Haven't found a signal that is emitted when a new tag is added. That should work though
-- since you can't use a layout on a tag that you haven't selected previously
tag.connect_signal("property::selected", function(t)
    if not t.top_idx then
        t.top_idx = 1
    end
end)

function ultrawide_mstab.arrange(p)
    local area = p.workarea
    local t = p.tag or screen[p.screen].selected_tag
    local s = t.screen
    local mwfact = t.master_width_factor
    local nmaster = math.min(t.master_count, #p.clients)
    local nslaves = #p.clients - nmaster

    local useless_gap_h = (area.width - beautiful.ultrawide_mstab_area_width) / 2
    useless_gap_h = useless_gap_h > 0 and useless_gap_h or 0

    local master_area_width = area.width * mwfact
    local slave_area_width = area.width - master_area_width

    -- Special case: No masters -> full screen slave width
    if nmaster == 0 then
        master_area_width = 1
        slave_area_width = area.width
    end

    -- Iterate through masters
    for idx = 1, nmaster do
        local c = p.clients[idx]
        local g = {
            x = area.x + useless_gap_h,
            y = area.y + (idx - 1) * (area.height / nmaster),
            width = master_area_width - useless_gap_h,
            height = area.height / nmaster,
        }
        p.geometries[c] = g
    end

    -- Iterate through slaves
    -- (also creates a list of all slave clients)
    local slave_clients = {}
    for idx = 1, nslaves do
        local c = p.clients[idx + nmaster]
        slave_clients[#slave_clients + 1] = c
        if c == client.focus then
            t.top_idx = #slave_clients
        end
        local g = {
            x = area.x + master_area_width,
            y = area.y,
            width = slave_area_width - useless_gap_h,
            height = area.height,
        }
        p.geometries[c] = g
    end
end

return ultrawide_mstab
