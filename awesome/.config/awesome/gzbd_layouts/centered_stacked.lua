-- Based on standard maximized layout from awful.
-- Adds wide horizontal padding, and slight vertical padding to center clients.
-- Useful when using ultra-wide monitor. You have clients in the center of a
-- screen, unlike left or right side of a monitor and moving your eyeballs.

--- The centered_stacked layout layoutbox icon.
-- @beautiful beautiful.layout_centered_stacked
-- @param surface
-- @see gears.surface

local beautiful = require("beautiful")

local centered_stacked = {}

local function center(p)
    local area = p.workarea
    local useless_gap_h = (area.width - beautiful.centered_stacked_client_width) / 2
    useless_gap_h = useless_gap_h > 0 and useless_gap_h or 0
    local useless_gap_v = (area.height - beautiful.centered_stacked_client_height) / 2
    useless_gap_v = useless_gap_v > 0 and useless_gap_v or 0

    for _, c in pairs(p.clients) do
        local g = {
            x = area.x + useless_gap_h,
            y = area.y + useless_gap_v,
            width = area.width - 2 * useless_gap_h,
            height = area.height - 2 * useless_gap_v,
        }
        p.geometries[c] = g
    end
end

centered_stacked.name = "centered_stacked"
function centered_stacked.arrange(p)
    return center(p)
end

return centered_stacked
