#!/bin/sh
# Copyright (C) 2020  Grzegorz Bednarski
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# cpu-fan.sh - shows current CPU fan spinning speed.
#
# Note: requires i8k kernel module on dell XPS 13 9360

speed=$(sensors | grep fan1: | cut -d " " -f 9)

if [ "$speed" != "" ]; then
    speed_round=$(echo "scale=1;$speed/1000" | bc -l )
    echo " $speed_round"
else
   echo " 0"
fi
