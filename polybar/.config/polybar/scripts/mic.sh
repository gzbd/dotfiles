#!/bin/sh
# Copyright (C) 2020  Grzegorz Bednarski
# Author: Grzegorz Bednarski
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# mic.sh - shows and sets default microphone state in pulseaudio

command=$1
source_id=$(pulsemixer --list-sources | grep Default | cut -d' ' -f3 | tr -d ',')

if [ "$command" == "toggle" ]; then
    pulsemixer --id $source_id --toggle-mute
elif [ "$command" == "volume-up" ]; then
    pulsemixer --id $source_id --change-volume +1
elif [ "$command" == "volume-down" ]; then
    pulsemixer --id $source_id --change-volume -1
fi

source_muted=$(pulsemixer --id $source_id --get-mute)
source_volume=$(pulsemixer --id $source_id --get-volume | cut -d' ' -f1)
icon=
if [ $source_muted == "1" ]; then
    icon=
fi

echo "$icon $source_volume%"
