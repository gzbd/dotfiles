#!/bin/sh
# Copyright (C) 2020  Grzegorz Bednarski
# Author: Grzegorz Bednarski
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# dunst.sh - toggle notification pause on click

command=$1

if [ "$command" == "toggle" ]; then
    dunstctl set-paused toggle

    if [ $(dunstctl is-paused) == "true" ]; then
        start="$(TZ=UTC0 printf '%(%s)T\n' '-1')"
        echo $start > /tmp/dunst-paused-start
    fi
fi

notifications_status=$(dunstctl is-paused)
display_text="ﮠ "
if [ $notifications_status == "true" ]; then
    start=`cat /tmp/dunst-paused-start`
    elapsedseconds=$(( $(TZ=UTC0 printf '%(%s)T\n' '-1') - start ))
    elapsed=$(TZ=UTC0 printf '%(%H:%M:%S)T\n' "$elapsedseconds")
    display_text="ﮡ  $elapsed"
fi

echo "$display_text"
