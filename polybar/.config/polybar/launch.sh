#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

MONITORS_PROFILE=$(autorandr | grep current | cut -d " " -f1)
EXTERNAL_MONITOR=DP1
if [ "$MONITORS_PROFILE" == "docked-home" ]
then
    EXTERNAL_MONITOR=DP1-3
fi

export EXTERNAL_MONITOR

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log /tmp/polybar3.log /tmp/polybar4.log
polybar primary-top >> /tmp/polybar1.log 2>&1 &
polybar primary-bottom >> /tmp/polybar2.log 2>&1 &
polybar secondary-top >> /tmp/polybar3.log 2>&1 &
polybar secondary-bottom >> /tmp/polybar4.log 2>&1 &

echo "Bars launched..."
