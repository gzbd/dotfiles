#! /bin/sh

pkill sxhkd
sxhkd &
sleep 2 && autorandr -c -d mobile &
picom -D 4 --vsync &
unclutter --root &
light-locker &
redshift-qt &
feh --bg-fill ~/Pictures/Wallpapers/cedric-letsch-pWyX-tVYMXI-unsplash.jpg &
dunst &
caffeine &
$HOME/.config/polybar/launch.sh &

# Iceberg colors definition
BACKGROUND=#161821
FOREGROUND=#C6C8D1
RED=#E27878
YELLOW=#E2A478
GREEN=#B4BE82
TEAL=#89B8C2
BLUE=#84A0C6
PINK=#A093C7
GRAY=#6B7089
BACKGROUND_LIGHT=#1E2132
BACKGROUND_LIGHT_TEXT=#444B71


MONITORS_NUMBER=$(xrandr -q | grep -w connected | wc -l)
MONITORS_PROFILE=$(autorandr | grep current | cut -d " " -f1)
if [ $MONITORS_PROFILE == "mobile" ]
then
    bspc monitor eDP1 -d web mail chat music term
elif [ $MONITORS_PROFILE == "docked" ]
then
    bspc monitor eDP1 -d web mail chat music term
    bspc monitor DP1 -d work
elif [ $MONITORS_PROFILE == "docked-home" ]
then
    bspc monitor eDP1 -d web mail chat music term
    bspc monitor DP1-3 -d work
fi

bspc config border_width         2
bspc config window_gap          24

bspc config top_monocle_padding    0
bspc config bottom_monocle_padding 0
bspc config left_monocle_padding   0
bspc config right_monocle_padding  0

bspc config remove_disabled_monitors   true
bspc config remove_unplugged_monitors  true
bspc config merge_overlapping_monitors true

bspc config split_ratio          0.52
bspc config borderless_monocle   true
bspc config gapless_monocle      true

bspc config normal_border_color  $BACKGROUND
bspc config active_border_color  $BACKGROUND
bspc config focused_border_color $BLUE

bspc config focus_follows_pointer   true
bspc config pointer_follows_focus   true
bspc config pointer_follows_monitor true

bspc rule -a Dictionary state=floating follow=on focus=on
bspc rule -a Zathura state=tiled follow=on focus=on
bspc rule -a Airtame state=tiled follow=on focus=on


shadow_node_state() {
    bspc subscribe node_state | while read -r _ _ _ node state status; do
        if [[ "$state" == "tiled" || "$state" == "floating" ]]; then
            case "$status" in
                off) xprop -id "$node" -remove _COMPTON_SHADOW;;
                on) xprop -id "$node" -f _COMPTON_SHADOW 32c -set _COMPTON_SHADOW 1;;
            esac
        fi
    done
}

shadow_node_add() {
    bspc subscribe node_add | while read -r _ _ _ _ node; do
        xprop -id "$node" -f _COMPTON_SHADOW 32c -set _COMPTON_SHADOW 1
    done
}

if ! pgrep -f "bspc subscribe node_state"; then
    shadow_node_state &
fi

if ! pgrep -f "bspc subscribe node_add"; then
    shadow_node_add &
fi
