# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.command import lazy
from libqtile import layout, hook

from typing import List  # noqa: F401

import subprocess

from colors import ICEBERG_COLORS
import keys as keybindings
import screens
import groups

mod = "mod4"


def count_monitors():
    cmd = subprocess.Popen('xrandr -q | grep -w connected', shell=True,
                           stdout=subprocess.PIPE)
    output = cmd.stdout.read()
    return len(output.decode('utf-8').split('\n')) - 1


MONITORS_COUNT = count_monitors()

mouse = keybindings.init_mouse()
keys = keybindings.init_keys(MONITORS_COUNT)
groups = groups.init_groups(MONITORS_COUNT)


layout_defaults = {
    'border_normal': ICEBERG_COLORS['background'],
    'border_focus': ICEBERG_COLORS['blue'],
    'border_width': 3,
    'margin': 5,
}

layouts = [
    layout.Max(),
    layout.Floating(**layout_defaults),
    layout.Matrix(**layout_defaults),
    layout.MonadTall(**layout_defaults),
    layout.MonadWide(**layout_defaults),
]

widget_defaults = dict(
    font='Hack Nerd Font',
    fontsize=16,
    padding=3,
    background=ICEBERG_COLORS['background'],
    foreground=ICEBERG_COLORS['foreground'],
)
extension_defaults = widget_defaults.copy()

screens = screens.init_screens(MONITORS_COUNT)


dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"


@hook.subscribe.startup_complete
def set_default_groups():
    if MONITORS_COUNT == 2:
        if screens[1].group.name != 'work':
            screens[1].cmd_toggle_group('work')

        if screens[0].group.name != 'web':
            screens[0].cmd_toggle_group('web')
    else:
        if screens[0].group.name != 'web':
            screens[0].cmd_toggle_group('web')

@hook.subscribe.screen_change
def restart(qtile, ev):
    qtile.cmd_restart()


@hook.subscribe.startup_once
def start_background_apps():
    processes = [
        ['picom', '-D', '4', '--vsync'],
        ['unclutter', '-root'],
        ['light-locker'],
        ['redshift-gtk'],
        ['wallpaper-changer'],
        ['xbindkeys'],
        ['dunst'],
        ['caffeine'],
    ]

    for p in processes:
        subprocess.Popen(p)


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
