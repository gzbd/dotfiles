from libqtile.widget import base, GroupBox
from libqtile import bar, hook

import os
import mailbox


class WindowList(base._TextBox):
    """
    A widget that displays current window title with bold font among other
    windows.
    """
    defaults = [
        ('separator', ' | ', 'separator text.'),
        ('markup', True),
        ('selected_color', '#ffffff'),
        ('selected_weight', 'bold'),
    ]

    def __init__(self, **config):
        base._TextBox.__init__(self, width=bar.STRETCH, **config)
        self.add_defaults(WindowList.defaults)

    def _configure(self, qtile, bar):
        base._TextBox._configure(self, qtile, bar)
        hook.subscribe.client_name_updated(self.update)
        hook.subscribe.focus_change(self.update)
        hook.subscribe.float_change(self.update)

    def button_press(self, x, y, button):
        self.bar.screen.group.cmd_next_window()

    def update(self, *args):
        names = []
        for w in self.bar.screen.group.windows:
            name = f'{w.name}'
            winfo = w.cmd_info()

            if winfo['maximized']:
                name = ' '  + name
            if winfo['minimized']:
                name = '  ' + name

            if w is self.bar.screen.group.current_window:
                name = f'<span weight="{self.selected_weight}" foreground="{self.selected_color}">{name}</span>'

            names.append(name)

        self.text = self.separator.join(names)
        self.bar.draw()


class FixedGroupBox(GroupBox):
    """
    A widget that displays only the visible_groups or hides groups specified in
    hidden_groups.
    """
    defaults = [
        (
            "visible_groups",
            None,
            "Groups that will be visible. "
            "Visible groups are identified by name not by their displayed label."
        ),
        (
            "hidden_groups",
            None,
            "Groups that will be hidden."
            "Hidden groups are identified by name not by their displayed label."
        ),
    ]

    def __init__(self, **config):
        GroupBox.__init__(self, **config)
        self.add_defaults(FixedGroupBox.defaults)


    @property
    def groups(self):
        groups = self.qtile.groups

        if self.visible_groups:
            return [g for g in groups if g.name in self.visible_groups]

        if self.hidden_groups:
            return [g for g in groups if g.name not in self.hidden_groups]

        return groups

    def _configure(self, qtile, bar):
        base._Widget._configure(self, qtile, bar)

        if self.fontsize is None:
            calc = self.bar.height - self.margin_y * 2 - \
                self.borderwidth * 2 - self.padding_y * 2
            self.fontsize = max(calc, 1)

        self.layout = self.drawer.textlayout(
            "",
            "ffffff",
            self.font,
            self.fontsize,
            self.fontshadow,
            markup=True,
        )
        self.setup_hooks()

    def draw(self):
        self.drawer.clear(self.background or self.bar.background)

        offset = self.margin_x
        for i, g in enumerate(self.groups):
            to_highlight = False
            is_block = (self.highlight_method == 'block')
            is_line = (self.highlight_method == 'line')

            bw = self.box_width([g])

            if self.group_has_urgent(g) and self.urgent_alert_method == "text":
                text_color = self.urgent_text
            elif g.windows:
                text_color = self.active
            else:
                text_color = self.inactive

            gname = g.label

            if g.screen:
                if self.highlight_method == 'text':
                    border = self.bar.background
                    text_color = self.this_current_screen_border
                    gname = f'<span weight="bold">{gname}</span>'
                else:
                    if self.bar.screen.group.name == g.name:
                        if self.qtile.current_screen == self.bar.screen:
                            border = self.this_current_screen_border
                            to_highlight = True
                        else:
                            border = self.this_screen_border
                    else:
                        if self.qtile.current_screen == g.screen:
                            border = self.other_current_screen_border
                        else:
                            border = self.other_screen_border
            elif self.group_has_urgent(g) and \
                    self.urgent_alert_method in ('border', 'block', 'line'):
                border = self.urgent_border
                if self.urgent_alert_method == 'block':
                    is_block = True
                elif self.urgent_alert_method == 'line':
                    is_line = True
            else:
                border = self.background or self.bar.background

            self.drawbox(
                offset,
                gname,
                border,
                text_color,
                highlight_color=self.highlight_color,
                width=bw,
                rounded=self.rounded,
                block=is_block,
                line=is_line,
                highlighted=to_highlight
            )
            offset += bw + self.spacing
        self.drawer.draw(offsetx=self.offset, width=self.width)



class Maildir(base.ThreadedPollText):
    """A simple widget showing the number of new mails in all maildir mailboxes"""
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ("maildir_path", "~/Mail", "path to the Maildir folder"),
    ]

    def __init__(self, **config):
        base.ThreadedPollText.__init__(self, **config)
        self.add_defaults(Maildir.defaults)

    def poll(self):
        """Scans the mailbox for new messages"""
        state = {}

        def to_maildir_fmt(paths):
            for path in iter(paths):
                yield path.rsplit(":")[0]


        maildir_path = os.path.expanduser(self.maildir_path)
        sub_folders = [d for d in os.listdir(maildir_path)
                       if not (d.lower() == 'spam' or d.startswith('.'))]

        for sub_folder in sub_folders:
            path = os.path.join(maildir_path, sub_folder)
            maildir = mailbox.Maildir(path)
            state[sub_folder] = 0

            for file in to_maildir_fmt(os.listdir(os.path.join(path, "new"))):
                if file in maildir:
                    state[sub_folder] += 1

        return str(sum(state.values()))
