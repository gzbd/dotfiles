ICEBERG_COLORS = {
    'background': '#161821',
    'foreground': '#c6c8d1',

    'red': '#e27878',
    'yellow': '#e2a478',
    'green': '#b4be82',
    'teal': '#89b8c2',
    'blue': '#84a0c6',
    'pink': '#a093c7',
    'gray': '#6b7089',

    'background_light': '#1e2132',
    'background_light_text': '#444b71',
}
