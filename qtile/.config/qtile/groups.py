from libqtile.config import Group


GROUPS_CFG = {
    'web': 0, # group name, screen pair
    'mail': 0,
    'chat': 0,
    'music': 0,
    'term': 0,
    'work': 1,
}


def init_groups(monitors_count):
    return [Group(name) for name in GROUPS_CFG.keys()]
