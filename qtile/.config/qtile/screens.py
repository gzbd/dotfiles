from libqtile import bar, widget
from libqtile.config import Screen

import subprocess

from colors import ICEBERG_COLORS
import widgets
import groups


MAIL_DIR = '~/.local/share/mail/personal'


def get_remaining_bat_time():
    output = subprocess.check_output(
        'acpi -b',
        stderr=subprocess.STDOUT,
        shell=True,
    )
    segments = output.decode('utf-8').split('\n')[1].split(',')
    if len(segments) < 3:
        return ''

    return segments[2][:6]


main_screen_widgets = [
    widget.Prompt(),
    widget.Spacer(length=bar.STRETCH),
    widget.TextBox(''),
    widgets.Maildir(
        maildir_path=MAIL_DIR,
        update_interval=10,
    ),
    widget.TextBox(''),
    widget.Volume(
        update_interval=1,
    ),
    widget.Spacer(length=10),
    widget.TextBox(''),
    widget.ThermalSensor(
        foreground=ICEBERG_COLORS['foreground'],
        foreground_alert=ICEBERG_COLORS['red'],
    ),
    widget.Spacer(length=10),
    widget.TextBox(''),
    widget.Net(
        interface='wlo1',
    ),
    widget.Spacer(length=10),
    widget.TextBox(''),
    widget.Wlan(
        interface='wlo1',
    ),
    widget.Spacer(length=10),
    widget.TextBox(''),
    widget.Memory(
        update_interval=10,
        fmt='{MemUsed}MB',
    ),
    widget.Spacer(length=10),
    widget.TextBox(''),
    widget.Pacman(
        unavailable=ICEBERG_COLORS['gray'],
    ),
    widget.Spacer(length=10),
    widget.TextBox(''),
    widget.Battery(
        update_interval=10,
        low_percentage=0.2,
        format='{percent:2.0%} {watt:.2f} W',
    ),
    widget.GenPollText(
        update_interval=10,
        func=get_remaining_bat_time,
    ),
    widget.Spacer(length=10),
    widget.Systray(),
    widget.Spacer(length=10),
    widget.Clock(
        format='%A %d %B %H:%M',
    ),
]


def init_screens(monitors_count):
    if monitors_count == 1:
        return [
            Screen(
                top=bar.Bar([
                    widgets.FixedGroupBox(
                        disable_drag=True,
                        active=ICEBERG_COLORS['blue'],
                        inactive=ICEBERG_COLORS['gray'],
                        highlight_method='text',
                        this_current_screen_border=ICEBERG_COLORS['foreground']),
                    *main_screen_widgets,
                ],
                    32,
                    background=ICEBERG_COLORS['background'],
                ),
                bottom=bar.Bar([
                       widgets.WindowList(
                           selected_color=ICEBERG_COLORS['blue']
                       ),
                       widget.CurrentLayoutIcon(scale=0.75)],
                       32,
                       background=ICEBERG_COLORS['background'],
               ),
            )
        ]
    return [
        Screen(
            top=bar.Bar([
                widgets.FixedGroupBox(
                    visible_groups=[
                        g for g, screen in groups.GROUPS_CFG.items()
                        if screen == 0
                    ],
                    disable_drag=True,
                    active=ICEBERG_COLORS['blue'],
                    inactive=ICEBERG_COLORS['gray'],
                    highlight_method='text',
                    this_current_screen_border=ICEBERG_COLORS['foreground']),
                widget.Spacer(length=bar.STRETCH),
                widget.Systray(),
                widget.Clock(format='%A %d %B %H:%M')],
                32,
                background=ICEBERG_COLORS['background']
            ),
            bottom=bar.Bar([
                   widgets.WindowList(
                       selected_color=ICEBERG_COLORS['blue']
                   ),
                   widget.CurrentLayoutIcon(scale=0.75)],
                   32,
                   background=ICEBERG_COLORS['background'],
           ),
        ),
        Screen(
            top=bar.Bar([
                widgets.FixedGroupBox(
                    hidden_groups=[
                        g for g, screen in groups.GROUPS_CFG.items()
                        if screen == 0
                    ],
                    disable_drag=True,
                    active=ICEBERG_COLORS['blue'],
                    inactive=ICEBERG_COLORS['gray'],
                    highlight_method='text',
                    this_current_screen_border=ICEBERG_COLORS['foreground'],
                ),
                *main_screen_widgets,
            ],
                32,
                background=ICEBERG_COLORS['background'],
            ),
            bottom=bar.Bar([
                widgets.WindowList(
                    selected_color=ICEBERG_COLORS['blue']
                ),
                widget.CurrentLayoutIcon(scale=0.75)],
                32,
                background=ICEBERG_COLORS['background'],
            ),
        ),
    ]

