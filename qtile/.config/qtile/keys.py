from libqtile.command import lazy
from libqtile.config import Key, Drag, Click
from libqtile.log_utils import logger

import groups


mod = "mod4"
alt = "mod1"


def get_screen_groups(qtile):
    current_screen_index = qtile.current_screen.index

    if len(qtile.screens) == 1:
        return qtile.groups

    gnames = [g for g, screen in groups.GROUPS_CFG.items()
              if screen == current_screen_index]
    grps = [g for g in qtile.groups if g.name in gnames]

    base_gnames = [g for g in groups.GROUPS_CFG.keys()]
    base_grps = [g for g in qtile.groups if g.name in base_gnames]

    if current_screen_index == 1:
        # add dynamically created groups
        # order in dynamic_groups matters.
        dynamic_groups = [g for g in qtile.groups if g not in base_grps]
        grps.extend(dynamic_groups)

    return grps


def switch_to_next_group():
    @lazy.function
    def f(qtile):
        current_group = qtile.current_group
        groups = get_screen_groups(qtile)

        current_index = groups.index(current_group)
        next_index = 0 if len(groups) - 1 == current_index else current_index + 1
        next_group = groups[next_index]
        qtile.current_screen.set_group(next_group)
    return f


def switch_to_previous_group():
    @lazy.function
    def f(qtile):
        current_group = qtile.current_group
        groups = get_screen_groups(qtile)

        current_index = groups.index(current_group)
        next_index = len(groups) - 1 if current_index == 0 else current_index - 1
        next_group = groups[next_index]
        qtile.current_screen.set_group(next_group)
    return f


def switch_to_group(index):
    @lazy.function
    def f(qtile):
        groups = get_screen_groups(qtile)

        try:
            qtile.current_screen.set_group(groups[index])
        except ValueError:
            return
    return f


def move_window_to_group(index):
    @lazy.function
    def f(qtile):
        groups = get_screen_groups(qtile)

        group = groups[index]
        qtile.current_window.togroup(group.name)
    return f


def new_group():
    @lazy.function
    def f(qtile):
        def create_group(gname):
            qtile.cmd_addgroup(gname)

        prompt = qtile.widgets_map['prompt']
        prompt.start_input('Group name', create_group)
    return f


def del_group():
    @lazy.function
    def f(qtile):
        qtile.cmd_delgroup(qtile.current_screen.group.name)
    return f


def init_keys(monitors_count):
    keys = [
        # Switch between windows in current stack pane
        Key([mod], "k", lazy.layout.down()),
        Key([mod], "j", lazy.layout.up()),

        # Move windows up or down in current stack
        Key([mod, "control"], "k", lazy.layout.shuffle_down()),
        Key([mod, "control"], "j", lazy.layout.shuffle_up()),

        # Switch window focus to other pane(s) of stack
        Key([mod], "space", lazy.layout.next()),

        # Swap panes of split stack
        Key([mod, "shift"], "space", lazy.layout.rotate()),

        Key([mod], "m", lazy.window.toggle_maximize()),
        Key([mod], "n", lazy.window.toggle_minimize()),

        # Toggle between split and unsplit sides of stack.
        # Split = all windows displayed
        # Unsplit = 1 window displayed, like Max layout, but still with
        # multiple stack panes
        Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
        Key([mod], "Return", lazy.spawn("alacritty")),

        # Toggle between different layouts as defined below
        Key([mod], "Tab", lazy.next_layout()),
        Key([mod, "shift"], "c", lazy.window.kill()),

        Key([mod, "control"], "r", lazy.restart()),
        Key([mod, "control"], "q", lazy.shutdown()),
        Key([mod], "x", lazy.spawn('rofi -show combi -combi-modi \"window,drun\" -modi combi -show-icons')),

        Key([mod], "Right",  switch_to_next_group()),
        Key([mod], "Left", switch_to_previous_group()),

        Key([mod, "shift"], "n", new_group()),
        Key([mod, "shift"], "d", del_group()),

        Key([], "XF86MonBrightnessUp", lazy.spawn('xbacklight -inc 10')),
        Key([], "XF86MonBrightnessDown", lazy.spawn('xbacklight -dec 10')),

        Key([mod, 'control'], "l", lazy.spawn('light-locker-command -l')),
        Key([alt], 'm', lazy.spawn('amixer set Master toggle')),
    ]


    for index in [0,1,2,3,4,5,6,7,8]:
        keys.extend([
            # mod1 + letter of group = switch to group
            Key([mod], str(index+1), switch_to_group(index)),

            # mod1 + shift + letter of group = switch to & move focused window to group
            Key([mod, "shift"], str(index+1), move_window_to_group(index)),
        ])


    if monitors_count == 2:
        keys.extend([
            Key([mod, "control"], "Left", lazy.to_screen(1)),
            Key([mod, "control"], "Right", lazy.to_screen(0)),
        ])

    return keys


def init_mouse():
    return [
        Drag([mod], "Button1", lazy.window.set_position_floating(),
             start=lazy.window.get_position()),
        Drag([mod], "Button3", lazy.window.set_size_floating(),
             start=lazy.window.get_size()),
        Click([mod], "Button2", lazy.window.bring_to_front())
    ]
