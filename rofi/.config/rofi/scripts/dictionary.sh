#!/bin/sh
# Copyright (C) 2020  Grzegorz Bednarski
# Author: Grzegorz Bednarski
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# dictionary.sh - custom modi to pass word to dict. Takes two arguments language
# and the word. When word arguments starts with word: prefix the word after the
# prefix is passed to dict, otherwise dict files is used to list possible word
# completions (at least two characters are needed).


LANG=$1
WORD=$2
WORD_LEN=${#WORD}
if [ "${WORD:0:5}" = word: ]
then
    DICT_WORD=${WORD:5}
    LANG=en_US.utf8 alacritty --class Dictionary,Dictionary -e bash -c "dict $DICT_WORD | less" &> /dev/null &
    exit
fi

if [ $WORD_LEN -gt 1 ]
then
    if [ $1 == "PL" ]
    then
        grep ^$WORD /usr/share/dict/polish | sed -e 's/^/word:/'
    else
        grep ^$WORD /usr/share/dict/american-english | sed -e 's/^/word:/'
    fi
fi
