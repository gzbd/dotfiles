#!/bin/bash
# Copyright (c) 2020, Grzegorz Bednarski
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#    bspwm_desktop_launcher.sh - lists all BSPWM desktops or creates new one.
#    To list all desktops in format below do not pass any arguments
#    desktop: $MONITOR_NAME    $DESKTOP_NAME     $DESKTOP_ID
#
#    ex. ./bspwm_new_desktop.sh
#
#    Note: Tabs are used as separators between $MONITOR_NAME $DESKTOP_NAME and
#    $DESKTOP_ID.
#
#    To focus an existing desktop pass a line from listing output to the script.
#
#    ex. ./bspwm_switch_desktop.sh "desktop: eDP1      browser    0x0040000B"
#
#    this will select 0x0040000B desktop.
#
#    To create a new desktop pass only the new desktop name as argument.
#
#    ex. ./bspwm_new_desktop.sh "my new desktop"
#
#   .NOTE: if the desktop name ends with ! (exclamation mark) it will be striped
#    from the desktop name. This is a hack for rofi launcher to force creating
#    new desktop with name that already is used.

function gen_desktops()
{

    monitors=`bspc query -M --names`
    for monitor in $monitors
    do
        desktops_count=`bspc query -D -m $monitor | wc -l`
        first_column=`paste -d" " <(yes "desktop:" | head -n $desktops_count) <(yes $monitor | head -n $desktops_count)`
        paste <(echo "$first_column") <(bspc query -D -m $monitor --names) <(bspc query -D -m $monitor)
    done
}

if [ -z "$@" ]
then
gen_desktops;
else
    desktop=$@
    if [ "${desktop:0:8}" = desktop: ]
    then
        desktop_id=`echo ${desktop:8} | cut -d" " -f3`
        bspc desktop -f $desktop_id
        exit
    else
        desktop_name_len=$((${#desktop}))
        last_char="${desktop:$desktop_name_len-1:1}"
        if [ "$last_char" = "!" ]
        then
            desktop="${desktop::$desktop_name_len-1}"
        fi
        bspc monitor --add-desktops "${desktop}"
    fi
fi

