# Path to your oh-my-zsh installation.
# export ZSH=/usr/share/oh-my-zsh
# export ZSH_CACHE_DIR=$HOME/.zsh-cache

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/usr/share/zsh

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# plugins=(ssh-agent zsh-syntax-highlighting aws)

# ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

# source $ZSH/oh-my-zsh.sh

export EDITOR="nvim"
export BROWSER="firefox"
export PATH="$PATH:/home/gzbd/.bin"
export PATH="$PATH:/home/gzbd/.gem/ruby/2.3.0/bin"
export PATH="$PATH:/home/gzbd/.gem/ruby/2.4.0/bin"
export GOPATH="$HOME/go"
export PATH="$PATH:$HOME/go/bin"
export PATH="$PATH:/opt/pact/bin"
export PATH="$PATH:/home/gzbd/.local/bin"

HOSTNAME=$(hostnamectl --static hostname)
if [ "$HOSTNAME" = "t14s" ]; then
    source ~/.t14s-gzbd.zsh
fi

export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
setopt HIST_IGNORE_DUPS
setopt EXTENDED_HISTORY
setopt SHARE_HISTORY
export MANPAGER='nvim +Man!'

export ELECTRON_FORCE_WINDOW_MENU_BAR=1 # makes electron apps consume less CPU resources

alias v='nvim'
alias n='neovide --multigrid --nofork'
alias ls="exa -la --group-directories-first --icons"
alias l="ls"
alias irssi='screen irssi'
alias feh="feh -d -Z"
alias vg="v -c Flog"
alias e="emacs -nw"
alias git-remove-merged-branches="git branch --merged master | grep -v 'master$' | xargs git branch -d"
alias git-remove-non-origin-branches="git branch -r | awk '{print $1}' | egrep -v -f /dev/fd/0 <(git branch -vv | grep origin) | awk '{print $1}' | xargs git branch -d"
alias rofi="rofi -show combi -combi-modi \"window,drun\" -modi combi -show-icons"
alias gonc="cd ~/go/src/bitbucket.org/nordcloud"
alias m="TERM=alacritty-direct neomutt"
alias ag="ag --color-line-number='1;32' --color-match='30;44' --color-path='1;38;5;109'"
alias cal="cal -m -w"
alias vimdiff="nvim -d"
alias z="zathura"

export LS_COLORS="di=1;36:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43"

# Always show menu select for completion
zstyle ':completion:*' menu yes select
# Add LS_COLORS to completion highlight
zstyle ":completion:*:default" list-colors ${(s.:.)LS_COLORS} "ma=\e[0;30;42"
# case insensitve completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
# for all completions: show comments when present
zstyle ':completion:*' verbose yes

autoload -Uz compinit && compinit

export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
--color=fg:#c6c8d1,bg:#1e2132,hl:#b4be82
--color=fg+:#c6c8d1,bg+:#1e2132,hl+:#b4be82
--color=info:#89b8c2,prompt:#b4be82,pointer:#b4be82
--color=marker:#89b8c2,spinner:#89b8c2,header:#e2a478'

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

source /usr/share/nvm/init-nvm.sh

eval "$(starship init zsh)"

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/terraform terraform

# use vi mode key-bindings
bindkey -v
