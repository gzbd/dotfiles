# Bluemoon colorscheme, taken from neovim config
#local bg_dark        = '#1b1e2b'
#local bg             = '#292d3e'
#local bg_light       = '#32374d'
#local bg_lighter     = '#444267'
#local grey           = '#8796b0'
#local grey_dark      = '#353b52'
#local red            = '#d06178'
#local heavy_red      = '#e61f44'
#local green          = '#b4c4b4'
#local green_high     = '#bcd9c4'
#local blue           = '#959dcb'
#local blue_light     = '#b8bcf3'
#local yellow         = '#cfcfbf'
#local yellow_light   = '#dfdf9f'
#local orange         = '#b4b4b4'
#local orange_light   = '#c2b0b0'
#local purple         = '#b9a3eb'
#local cyan_dark      = '#89bbdd'
#local cyan           = '#89ddff'
#local fg             = '#a6accd'
#local fg_light       = '#fbfbfb'
#local fg_dark        = '#676e96'
#local hollow         = '#424760'
#local hollow_lighter = '#30354e'
#local white          = '#ffffff'

# Default index colors:
color index '#b8bcf3' default '.*'
color index_author '#b8bcf3' default '.*'
color index_subject '#b8bcf3' default '.*'
color index_number '#b8bcf3' default
color tree '#676e96' default

# New mail is boldened:
color index bold '#cfcfbf' default "~N"
color index_author bold '#cfcfbf' default "~N"
color index_subject bold '#cfcfbf' default "~N"

# Tagged emails
color index '#a6accd' '#32374d' "~T"
color index_author '#a6accd' '#32374d' "~T"
color index_subject '#a6accd' '#32374d' "~T"

# Other colors and aesthetic settings:
mono bold bold
mono underline underline
mono indicator reverse
mono error bold
color normal default default
color indicator black white
color sidebar_highlight '#cfcfbf' black
color sidebar_divider '#676e96' black
color sidebar_flagged '#d06178' black
color sidebar_new '#cfcfbf' black
color sidebar_ordinary '#b8bcf3' black
color normal '#a6accd' default
color error '#d06178' default
color tilde '#676e96' default
color message '#89ddff' default
color markers '#676e96' white
color attachment '#cfcfbf' default
color search '#292d3e' '#dfdf9f'
color status '#c2b0b0' black
# color hdrdefault '#b4c4b4' default
color quoted  '#b8bcf3' default
color quoted1 '#c2b0b0' default
color quoted2 '#b8bcf3' default
color quoted3 '#c2b0b0' default
color quoted4 '#b8bcf3' default
color quoted5 '#c2b0b0' default
color signature '#cfcfbf' default
color bold '#fbfbfb' default
color underline '#fbfbfb' default
color normal default default

# Regex highlighting:
color header standout '#1b1e2b' '#89bbdd' ".*"
color body '#b9a3eb' default "[\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+" # Email addresses
color body underline '#89bbdd' default "([a-z][a-z0-9+-]*://(((([a-z0-9_.!~*'();:&=+$,-]|%[0-9a-f][0-9a-f])*@)?((([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?|[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)(:[0-9]+)?)|([a-z0-9_.!~*'()$,;:@&=+-]|%[0-9a-f][0-9a-f])+)(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?(#([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?|(www|ftp)\\.(([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?(:[0-9]+)?(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?(#([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?)[^].,:;!)? \t\r\n<>\"]" # URLs
